﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using Komunalka.DAL;
using Komunalka.Models.Requests;

namespace Komunalka.Services
{
    public abstract class BehBase : Behavior<Button>
    {
        protected override void OnAttached()
        {
            AssociatedObject.Click += OnClick;
        }
        protected override void OnDetaching()
        {
            AssociatedObject.Click -= OnClick;
        }
        protected abstract void OnClick(object sender, RoutedEventArgs e);
    }

    public class InfoRequestBeh: BehBase
    {
        protected override void OnClick(object sender, RoutedEventArgs e)
        {
            var dk = (sender as Button)?.DataContext as Request;
            if (dk != null)
            {
                new RequestInfoView(dk).Show();
            }
            //
        }

    }
}
