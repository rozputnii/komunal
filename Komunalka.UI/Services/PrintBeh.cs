using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using GemBox.Document;
using Komunalka.DAL;
using Microsoft.Win32;

namespace Komunalka.Services
{
    public class PrintBeh : Behavior<Button>
    {
        protected sealed override void OnAttached()
        {
            AssociatedObject.Click += OnClick;
        }

        private  void OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var dk = (sender as Button)?.DataContext as Request;
                if (dk != null)
                {
                    var dlg = new SaveFileDialog();
                    dlg.Filter = "Word files (*.docx)|*.docx|All files (*.*)|*.*";
                    dlg.RestoreDirectory = true;
                    dlg.ShowDialog();
                    var path = dlg.FileName;
                    if (File.Exists(path))
                        File.Delete(path);
                    // If using Professional version, put your serial key below.
                    ComponentInfo.SetLicense("FREE-LIMITED-KEY");

                    var document = new DocumentModel();

                    document.Sections.Add(
                        new Section(document,
                            new Paragraph(document,
                                new SpecialCharacter(document, SpecialCharacterType.Tab),
                                new SpecialCharacter(document, SpecialCharacterType.Tab),
                                new Run(document, $"������ �{dk.Id}"),
                                new SpecialCharacter(document, SpecialCharacterType.LineBreak),
                                new Run(document, $"�����: {dk.Name}"),
                                new SpecialCharacter(document, SpecialCharacterType.LineBreak),
                                new Run(document, $"ϲ�: {dk.PIB}"),
                                new SpecialCharacter(document, SpecialCharacterType.LineBreak),
                                new Run(document, $"������: {dk.Adress}"),
                                new SpecialCharacter(document, SpecialCharacterType.LineBreak),
                                new Run(document, $"�������: {dk.Phone}"),
                                new SpecialCharacter(document, SpecialCharacterType.LineBreak),
                                new Run(document, $"��� ������: {dk.KindOfRequest?.Name}"),
                                new SpecialCharacter(document, SpecialCharacterType.LineBreak),
                                new Run(document, $"��� ����: {dk.TypeOfWork?.Name}"),
                                new SpecialCharacter(document, SpecialCharacterType.LineBreak),
                                new Run(document, $"�������� �������: {dk.WorkGroup?.Name}"),
                                new SpecialCharacter(document, SpecialCharacterType.LineBreak),
                                new Run(document,
                                    $"�������: {dk.Employees.Aggregate(string.Empty, (current, employee) => current + employee.Name + "; ")}"),
                                new SpecialCharacter(document, SpecialCharacterType.LineBreak),
                                new Run(document, $"����: {dk.Date}"),
                                new SpecialCharacter(document, SpecialCharacterType.LineBreak)
                                )));

                    document.Save(path);
                    var cmd = "explorer.exe";
                    var arg = "/select, " + path;
                    Process.Start(cmd, arg);
                }
            }
            catch
            {
                
            }
        }

        protected sealed override void OnDetaching()
        {
            AssociatedObject.Click -= OnClick;
        }
    }
}