﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Threading;

namespace Komunalka.Services
{
    public class RangeToVCon : IValueConverter
    {
        static RangeToVCon()
        {
            Vis0 = new RangeToVCon(0);
            Vis1 = new RangeToVCon(1);
            Vis2 = new RangeToVCon(2);
        }

        private RangeToVCon(int v)
        {
            _val = v;
        }

        private readonly int _val;
        public static RangeToVCon Vis0 { get; }
        public static RangeToVCon Vis1 { get; }
        public static RangeToVCon Vis2 { get; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (_val)
            {
                case 0:
                {
                    if ((int) value != 1 && (int) value != 2) return Visibility.Visible;
                        return Visibility.Collapsed;
                }
                case 1:
                {
                    if ((int) value == 1) return Visibility.Visible;
                    return Visibility.Collapsed;
                }
                case 2:
                {
                    if ((int) value == 2) return Visibility.Visible;
                    return Visibility.Collapsed;
                }
                default:
                {
                    if ((int) value != 1 && (int) value != 2) return Visibility.Visible;
                    return Visibility.Collapsed;
                }
            }



            
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

    public static class ActionExt
    {
        public static Task RunWithDispatcher(this Action action)
        {
            return new Action(action.CallWithDispatcher).Run();
        }

        public static Task RunWithDispatcher(this Action action, int millisecondsTimeout)
        {
            return new Action(action.CallWithDispatcher).Run(millisecondsTimeout);
        }

        public static void CallWithDispatcher(this Action action)
        {
            action.CallWithDispatcher(DispatcherPriority.Normal);
        }

        public static void CallWithDispatcher(this Action action, DispatcherPriority priority)
        {
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, action);
        }
        public static Task Run(this Action action)
        {
            return Task.Run(action);
        }

        public static Task Run(this Action action, int millisecondsTimeout)
        {
            return Task.Delay(millisecondsTimeout).ContinueWith(task => { action(); });
        }
    }
}
