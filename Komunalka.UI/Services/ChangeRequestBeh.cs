using System.Windows;
using System.Windows.Controls;
using Komunalka.DAL;
using Komunalka.Models.Requests;

namespace Komunalka.Services
{
    public class ChangeRequestBeh : BehBase
    {
        protected override void OnClick(object sender, RoutedEventArgs e)
        {
            var dk = (sender as Button)?.DataContext as Request;
            if (dk != null)
            {
                if (dk.Status != 2)
                {
                    new RequestChangeView(dk).Show();
                }
                else
                {
                    MessageBox.Show( "��������� ���������� �������� ������", "�����!", MessageBoxButton.OK,
                        MessageBoxImage.Information);
                }

                
            }
        }
    }
}