using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;

namespace Komunalka.Services
{
    public class ToWorkBeh : Behavior<Button>
    {
        protected sealed override void OnAttached()
        {
            AssociatedObject.Click += OnClick;
        }
       
        private async void OnClick(object sender, RoutedEventArgs e)
        {
            var dk = (sender as Button)?.DataContext as Request;
            if (dk != null)
            {
                if (dk.Status < 2)
                {
                    dk.Status++;
                    if (dk.Status == 2) dk.DateEnd = DateTime.Now;

                    await Repo.Requests.Update(dk);
                    Repo.Employees.OnChanged();
                }
                else
                {
                    ToArchive(dk);
                }
            }
        }

        public static async void ToArchive(Request request)
        {
            var sa = request.Employees.Aggregate(string.Empty, (current, employee) => current + employee.Name + "; ");
            var ra = new RequestInArchive
            {
                Date = request.Date,
                WorkGroups = request.WorkGroup?.Name,
                Employees = sa,
                Adress = request.Adress,
                DateEnd = request.DateEnd,
                Description = request.Description,
                KindOfRequestName = request.KindOfRequest?.Name,
                Name = request.Name,
                PIB = request.PIB,
                Phone = request.Phone,
                Status = request.Status.ToString(),
                TypeOfWorkName = request.TypeOfWork?.Name,
                RequestId = request.Id,
            };
            request.InArchive = true;
            await Repo.RequestInArchives.Add(ra);
            await Repo.Requests.Update(request);
            
        }
    }
}
