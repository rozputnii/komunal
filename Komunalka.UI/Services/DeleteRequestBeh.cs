using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;

namespace Komunalka.Services
{
    public class DeleteRequestBeh : Behavior<Button>
    {
        protected sealed override void OnAttached()
        {
            AssociatedObject.Click += OnClick;
        }

        private async void OnClick(object sender, RoutedEventArgs e)
        {
            var dk = (sender as Button)?.DataContext as Request;
            if (dk != null)
            {
                if (dk.Status != 2)
                {
                    await Repo.Requests.Delete(dk);
                }
                else
                {
                    MessageBox.Show( "��������� �������� �������� ������","�����!", MessageBoxButton.OK,
                        MessageBoxImage.Information);
                }
            }
        }

        protected sealed override void OnDetaching()
        {
            AssociatedObject.Click -= OnClick;
        }
    }
}