using System;
using System.Globalization;
using System.Windows.Data;

namespace Komunalka.Services
{
    public class ConvertDay : IValueConverter
    {
        static ConvertDay()
        {
            Day = new ConvertDay();
        }
        public static ConvertDay Day { get; }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var date = (DateTime) value;
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    return "�����";
                case DayOfWeek.Monday:
                    return "��������";
                case DayOfWeek.Tuesday:
                    return "³������";
                case DayOfWeek.Wednesday:
                    return "������";
                case DayOfWeek.Thursday:
                    return "������";
                case DayOfWeek.Friday:
                    return "�'������";
                case DayOfWeek.Saturday:
                    return "������";
                default:
                    return "...";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
    public class ConvertShortDate : IValueConverter
    {
        static ConvertShortDate()
        {
            Date = new ConvertShortDate();
        }
        public static ConvertShortDate Date { get; }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var date = (DateTime)value;
            return $"{date:dd-MM-yy}";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
    //public class CoundDoneConv : IValueConverter
    //{
    //    static CoundDoneConv()
    //    {
    //        CuntOnDone=new CoundDoneConv(2);
    //        Cunt=new CoundDoneConv(0);
    //    }

    //    public CoundDoneConv(int i)
    //    {
    //        _i = i;
    //    }
    //    public static CoundDoneConv CuntOnDone { get; }
    //    public static CoundDoneConv Cunt { get; }

    //    private readonly int _i;

    //    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        var empl = value as Employee;
    //        if (empl == null) return 0;
    //        return _i == 2 ? empl.Requests?.Count(request => request.Status.Equals(2)) ?? 0+empl.WorkGroup?.Requests?.Count(request => request.Status.Equals(2))??0 : empl.Requests?.Count ?? 0+empl.WorkGroup?.Requests?.Count() ?? 0;
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        return Binding.DoNothing;
    //    }
    //}
}