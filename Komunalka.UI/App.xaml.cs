﻿using System.Globalization;
using System.Threading;
using System.Windows;

namespace Komunalka
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            var k = new CultureInfo("uk-UA");
            CultureInfo.DefaultThreadCurrentCulture = k;
            CultureInfo.DefaultThreadCurrentUICulture = k;
            Thread.CurrentThread.CurrentCulture = k;
            Thread.CurrentThread.CurrentUICulture = k;
            FrameworkElement.LanguageProperty.OverrideMetadata(
                typeof(FrameworkElement),
                new FrameworkPropertyMetadata(
                    System.Windows.Markup.XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));

            base.OnStartup(e);
        }

    }

}
