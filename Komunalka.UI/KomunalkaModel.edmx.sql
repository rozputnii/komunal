
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/18/2016 18:19:39
-- Generated from EDMX file: E:\ok\testDiplopka\testDiplopka\KomunalkaModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Komunalka];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_EmployeeSpecialty]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Employees] DROP CONSTRAINT [FK_EmployeeSpecialty];
GO
IF OBJECT_ID(N'[dbo].[FK_KindOfRequestRequest]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Requests] DROP CONSTRAINT [FK_KindOfRequestRequest];
GO
IF OBJECT_ID(N'[dbo].[FK_TypeOfWorkRequest]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Requests] DROP CONSTRAINT [FK_TypeOfWorkRequest];
GO
IF OBJECT_ID(N'[dbo].[FK_ScheduleEmployee_Schedule]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ScheduleEmployee] DROP CONSTRAINT [FK_ScheduleEmployee_Schedule];
GO
IF OBJECT_ID(N'[dbo].[FK_ScheduleEmployee_Employee]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ScheduleEmployee] DROP CONSTRAINT [FK_ScheduleEmployee_Employee];
GO
IF OBJECT_ID(N'[dbo].[FK_ScheduleWorkGroup_Schedule]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ScheduleWorkGroup] DROP CONSTRAINT [FK_ScheduleWorkGroup_Schedule];
GO
IF OBJECT_ID(N'[dbo].[FK_ScheduleWorkGroup_WorkGroup]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ScheduleWorkGroup] DROP CONSTRAINT [FK_ScheduleWorkGroup_WorkGroup];
GO
IF OBJECT_ID(N'[dbo].[FK_EmployeeWorkGroup]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Employees] DROP CONSTRAINT [FK_EmployeeWorkGroup];
GO
IF OBJECT_ID(N'[dbo].[FK_WorkGroupRequest]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Requests] DROP CONSTRAINT [FK_WorkGroupRequest];
GO
IF OBJECT_ID(N'[dbo].[FK_ManagerRequest]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Requests] DROP CONSTRAINT [FK_ManagerRequest];
GO
IF OBJECT_ID(N'[dbo].[FK_EmployeeRequest_Employee]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[EmployeeRequest] DROP CONSTRAINT [FK_EmployeeRequest_Employee];
GO
IF OBJECT_ID(N'[dbo].[FK_EmployeeRequest_Request]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[EmployeeRequest] DROP CONSTRAINT [FK_EmployeeRequest_Request];
GO
IF OBJECT_ID(N'[dbo].[FK_EmployeeMaterialBase]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MaterialBases] DROP CONSTRAINT [FK_EmployeeMaterialBase];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialBaseMaterial]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Materials] DROP CONSTRAINT [FK_MaterialBaseMaterial];
GO
IF OBJECT_ID(N'[dbo].[FK_EmployeeInstrument]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments] DROP CONSTRAINT [FK_EmployeeInstrument];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Employees]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Employees];
GO
IF OBJECT_ID(N'[dbo].[Managers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Managers];
GO
IF OBJECT_ID(N'[dbo].[WorkGroups]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WorkGroups];
GO
IF OBJECT_ID(N'[dbo].[Requests]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Requests];
GO
IF OBJECT_ID(N'[dbo].[MaterialBases]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MaterialBases];
GO
IF OBJECT_ID(N'[dbo].[TypeOfWorks]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TypeOfWorks];
GO
IF OBJECT_ID(N'[dbo].[KindOfRequests]', 'U') IS NOT NULL
    DROP TABLE [dbo].[KindOfRequests];
GO
IF OBJECT_ID(N'[dbo].[Specialties]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Specialties];
GO
IF OBJECT_ID(N'[dbo].[Schedules]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Schedules];
GO
IF OBJECT_ID(N'[dbo].[Materials]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Materials];
GO
IF OBJECT_ID(N'[dbo].[Instruments]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Instruments];
GO
IF OBJECT_ID(N'[dbo].[ScheduleEmployee]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ScheduleEmployee];
GO
IF OBJECT_ID(N'[dbo].[ScheduleWorkGroup]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ScheduleWorkGroup];
GO
IF OBJECT_ID(N'[dbo].[EmployeeRequest]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EmployeeRequest];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Employees'
CREATE TABLE [dbo].[Employees] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Discharge] int  NULL,
    [Experience] int  NULL,
    [Address] nvarchar(max)  NULL,
    [Phone] nvarchar(max)  NULL,
    [SpecialtyId] int  NOT NULL,
    [RequestId] int  NULL,
    [ScheduleId] int  NULL,
    [WorkGroupId] int  NULL
);
GO

-- Creating table 'Managers'
CREATE TABLE [dbo].[Managers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Login] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'WorkGroups'
CREATE TABLE [dbo].[WorkGroups] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Requests'
CREATE TABLE [dbo].[Requests] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Adress] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [KindOfRequestId] int  NOT NULL,
    [TypeOfWorkId] int  NOT NULL,
    [Date] datetime  NOT NULL,
    [WorkGroupId] int  NOT NULL,
    [ManagerId] int  NULL
);
GO

-- Creating table 'MaterialBases'
CREATE TABLE [dbo].[MaterialBases] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [Exists] bit  NOT NULL,
    [EmployeeId] int  NULL
);
GO

-- Creating table 'TypeOfWorks'
CREATE TABLE [dbo].[TypeOfWorks] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'KindOfRequests'
CREATE TABLE [dbo].[KindOfRequests] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Specialties'
CREATE TABLE [dbo].[Specialties] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [IsChecked] bit  NOT NULL
);
GO

-- Creating table 'Schedules'
CREATE TABLE [dbo].[Schedules] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [TimeStart] datetime  NOT NULL,
    [TimeEnd] datetime  NOT NULL
);
GO

-- Creating table 'Materials'
CREATE TABLE [dbo].[Materials] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [Serviceable] bit  NOT NULL,
    [MaterialBaseId] int  NULL
);
GO

-- Creating table 'Instruments'
CREATE TABLE [dbo].[Instruments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [Serviceable] bit  NOT NULL,
    [EmployeeId] int  NULL
);
GO

-- Creating table 'ScheduleEmployee'
CREATE TABLE [dbo].[ScheduleEmployee] (
    [Schedules_Id] int  NOT NULL,
    [Employees_Id] int  NOT NULL
);
GO

-- Creating table 'ScheduleWorkGroup'
CREATE TABLE [dbo].[ScheduleWorkGroup] (
    [Schedules_Id] int  NOT NULL,
    [WorkGroups_Id] int  NOT NULL
);
GO

-- Creating table 'EmployeeRequest'
CREATE TABLE [dbo].[EmployeeRequest] (
    [Employees_Id] int  NOT NULL,
    [Requests_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Employees'
ALTER TABLE [dbo].[Employees]
ADD CONSTRAINT [PK_Employees]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Managers'
ALTER TABLE [dbo].[Managers]
ADD CONSTRAINT [PK_Managers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'WorkGroups'
ALTER TABLE [dbo].[WorkGroups]
ADD CONSTRAINT [PK_WorkGroups]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Requests'
ALTER TABLE [dbo].[Requests]
ADD CONSTRAINT [PK_Requests]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MaterialBases'
ALTER TABLE [dbo].[MaterialBases]
ADD CONSTRAINT [PK_MaterialBases]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TypeOfWorks'
ALTER TABLE [dbo].[TypeOfWorks]
ADD CONSTRAINT [PK_TypeOfWorks]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'KindOfRequests'
ALTER TABLE [dbo].[KindOfRequests]
ADD CONSTRAINT [PK_KindOfRequests]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Specialties'
ALTER TABLE [dbo].[Specialties]
ADD CONSTRAINT [PK_Specialties]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Schedules'
ALTER TABLE [dbo].[Schedules]
ADD CONSTRAINT [PK_Schedules]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Materials'
ALTER TABLE [dbo].[Materials]
ADD CONSTRAINT [PK_Materials]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments'
ALTER TABLE [dbo].[Instruments]
ADD CONSTRAINT [PK_Instruments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Schedules_Id], [Employees_Id] in table 'ScheduleEmployee'
ALTER TABLE [dbo].[ScheduleEmployee]
ADD CONSTRAINT [PK_ScheduleEmployee]
    PRIMARY KEY CLUSTERED ([Schedules_Id], [Employees_Id] ASC);
GO

-- Creating primary key on [Schedules_Id], [WorkGroups_Id] in table 'ScheduleWorkGroup'
ALTER TABLE [dbo].[ScheduleWorkGroup]
ADD CONSTRAINT [PK_ScheduleWorkGroup]
    PRIMARY KEY CLUSTERED ([Schedules_Id], [WorkGroups_Id] ASC);
GO

-- Creating primary key on [Employees_Id], [Requests_Id] in table 'EmployeeRequest'
ALTER TABLE [dbo].[EmployeeRequest]
ADD CONSTRAINT [PK_EmployeeRequest]
    PRIMARY KEY CLUSTERED ([Employees_Id], [Requests_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [SpecialtyId] in table 'Employees'
ALTER TABLE [dbo].[Employees]
ADD CONSTRAINT [FK_EmployeeSpecialty]
    FOREIGN KEY ([SpecialtyId])
    REFERENCES [dbo].[Specialties]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmployeeSpecialty'
CREATE INDEX [IX_FK_EmployeeSpecialty]
ON [dbo].[Employees]
    ([SpecialtyId]);
GO

-- Creating foreign key on [KindOfRequestId] in table 'Requests'
ALTER TABLE [dbo].[Requests]
ADD CONSTRAINT [FK_KindOfRequestRequest]
    FOREIGN KEY ([KindOfRequestId])
    REFERENCES [dbo].[KindOfRequests]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_KindOfRequestRequest'
CREATE INDEX [IX_FK_KindOfRequestRequest]
ON [dbo].[Requests]
    ([KindOfRequestId]);
GO

-- Creating foreign key on [TypeOfWorkId] in table 'Requests'
ALTER TABLE [dbo].[Requests]
ADD CONSTRAINT [FK_TypeOfWorkRequest]
    FOREIGN KEY ([TypeOfWorkId])
    REFERENCES [dbo].[TypeOfWorks]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TypeOfWorkRequest'
CREATE INDEX [IX_FK_TypeOfWorkRequest]
ON [dbo].[Requests]
    ([TypeOfWorkId]);
GO

-- Creating foreign key on [Schedules_Id] in table 'ScheduleEmployee'
ALTER TABLE [dbo].[ScheduleEmployee]
ADD CONSTRAINT [FK_ScheduleEmployee_Schedule]
    FOREIGN KEY ([Schedules_Id])
    REFERENCES [dbo].[Schedules]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Employees_Id] in table 'ScheduleEmployee'
ALTER TABLE [dbo].[ScheduleEmployee]
ADD CONSTRAINT [FK_ScheduleEmployee_Employee]
    FOREIGN KEY ([Employees_Id])
    REFERENCES [dbo].[Employees]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ScheduleEmployee_Employee'
CREATE INDEX [IX_FK_ScheduleEmployee_Employee]
ON [dbo].[ScheduleEmployee]
    ([Employees_Id]);
GO

-- Creating foreign key on [Schedules_Id] in table 'ScheduleWorkGroup'
ALTER TABLE [dbo].[ScheduleWorkGroup]
ADD CONSTRAINT [FK_ScheduleWorkGroup_Schedule]
    FOREIGN KEY ([Schedules_Id])
    REFERENCES [dbo].[Schedules]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [WorkGroups_Id] in table 'ScheduleWorkGroup'
ALTER TABLE [dbo].[ScheduleWorkGroup]
ADD CONSTRAINT [FK_ScheduleWorkGroup_WorkGroup]
    FOREIGN KEY ([WorkGroups_Id])
    REFERENCES [dbo].[WorkGroups]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ScheduleWorkGroup_WorkGroup'
CREATE INDEX [IX_FK_ScheduleWorkGroup_WorkGroup]
ON [dbo].[ScheduleWorkGroup]
    ([WorkGroups_Id]);
GO

-- Creating foreign key on [WorkGroupId] in table 'Employees'
ALTER TABLE [dbo].[Employees]
ADD CONSTRAINT [FK_EmployeeWorkGroup]
    FOREIGN KEY ([WorkGroupId])
    REFERENCES [dbo].[WorkGroups]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmployeeWorkGroup'
CREATE INDEX [IX_FK_EmployeeWorkGroup]
ON [dbo].[Employees]
    ([WorkGroupId]);
GO

-- Creating foreign key on [WorkGroupId] in table 'Requests'
ALTER TABLE [dbo].[Requests]
ADD CONSTRAINT [FK_WorkGroupRequest]
    FOREIGN KEY ([WorkGroupId])
    REFERENCES [dbo].[WorkGroups]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WorkGroupRequest'
CREATE INDEX [IX_FK_WorkGroupRequest]
ON [dbo].[Requests]
    ([WorkGroupId]);
GO

-- Creating foreign key on [ManagerId] in table 'Requests'
ALTER TABLE [dbo].[Requests]
ADD CONSTRAINT [FK_ManagerRequest]
    FOREIGN KEY ([ManagerId])
    REFERENCES [dbo].[Managers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ManagerRequest'
CREATE INDEX [IX_FK_ManagerRequest]
ON [dbo].[Requests]
    ([ManagerId]);
GO

-- Creating foreign key on [Employees_Id] in table 'EmployeeRequest'
ALTER TABLE [dbo].[EmployeeRequest]
ADD CONSTRAINT [FK_EmployeeRequest_Employee]
    FOREIGN KEY ([Employees_Id])
    REFERENCES [dbo].[Employees]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Requests_Id] in table 'EmployeeRequest'
ALTER TABLE [dbo].[EmployeeRequest]
ADD CONSTRAINT [FK_EmployeeRequest_Request]
    FOREIGN KEY ([Requests_Id])
    REFERENCES [dbo].[Requests]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmployeeRequest_Request'
CREATE INDEX [IX_FK_EmployeeRequest_Request]
ON [dbo].[EmployeeRequest]
    ([Requests_Id]);
GO

-- Creating foreign key on [EmployeeId] in table 'MaterialBases'
ALTER TABLE [dbo].[MaterialBases]
ADD CONSTRAINT [FK_EmployeeMaterialBase]
    FOREIGN KEY ([EmployeeId])
    REFERENCES [dbo].[Employees]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmployeeMaterialBase'
CREATE INDEX [IX_FK_EmployeeMaterialBase]
ON [dbo].[MaterialBases]
    ([EmployeeId]);
GO

-- Creating foreign key on [MaterialBaseId] in table 'Materials'
ALTER TABLE [dbo].[Materials]
ADD CONSTRAINT [FK_MaterialBaseMaterial]
    FOREIGN KEY ([MaterialBaseId])
    REFERENCES [dbo].[MaterialBases]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialBaseMaterial'
CREATE INDEX [IX_FK_MaterialBaseMaterial]
ON [dbo].[Materials]
    ([MaterialBaseId]);
GO

-- Creating foreign key on [EmployeeId] in table 'Instruments'
ALTER TABLE [dbo].[Instruments]
ADD CONSTRAINT [FK_EmployeeInstrument]
    FOREIGN KEY ([EmployeeId])
    REFERENCES [dbo].[Employees]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmployeeInstrument'
CREATE INDEX [IX_FK_EmployeeInstrument]
ON [dbo].[Instruments]
    ([EmployeeId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------