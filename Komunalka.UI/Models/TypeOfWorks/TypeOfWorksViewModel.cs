using System.Linq;
using System.Windows;
using System.Windows.Input;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.TypeOfWorks
{
    public class TypeOfWorksViewModel : ManyElementBase<TypeOfWork>
    {
        public TypeOfWorksViewModel() : base(Repo.TypeOfWorks)
        {
            SaveCommand=new Command(Save);
        }

        

        private void Save(object o)
        {
            try
            {
                Change();
                (o as Window)?.Close();
            }
            catch
            {
                
            }
        }

        public ICommand SaveCommand { get; set; }

        protected override async void Change()
        {
            var s = Repo.TypeOfWorks.GetAll?.Convert();

            if(s!=null)
                foreach (var typeOfWork in s)
                {
                    if (!Items.Contains(typeOfWork))
                        await Repo.TypeOfWorks.Delete(typeOfWork);
                }

            foreach (var typeOfWork in Items)
            {
                await Repo.TypeOfWorks.Update(typeOfWork);
            }

        }
    }
}