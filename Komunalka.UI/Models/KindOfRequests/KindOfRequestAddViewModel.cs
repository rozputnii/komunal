﻿using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.KindOfRequests
{
    public class KindOfRequestAddViewModel : AddBase<KindOfRequest>
    {
        public KindOfRequestAddViewModel() : base(Repo.KindOfRequests)
        {
        }
    }
}