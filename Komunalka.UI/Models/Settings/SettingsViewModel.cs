﻿using System.Linq;
using System.Windows.Input;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;
using Komunalka.Models.Manegers;
using static Komunalka.Properties.Settings;

namespace Komunalka.Models.Settings
{
    public class SettingsViewModel : ViewModelBase
    {
        public bool Loading { get; set; }

        public SettingsViewModel()
        {
            SaveCommand = new Command(async () =>
            {
                Loading = true;
                Default.IsFirstLoading = false;
                Default.ServerName = ServerName;
                Default.DbName = DbName;
                Default.UseLocalServer = UseLocalServer;
                Default.CreateDbIfNotExists = CreateDbIfNotExists;
                Default.Save();
                Repo.Provider.Settings.CreateDbIfNotExists = Default.CreateDbIfNotExists;
                Repo.Provider.Settings.ServerName = Default.ServerName;
                Repo.Provider.Settings.DbName = Default.DbName;
                Repo.Provider.Settings.UseLocalServer = Default.UseLocalServer;

                Repo.Provider.JustCreateContext();
                var r = await Repo.Provider.CreateIfNotExists(Default.CreateDbIfNotExists);
                
                Loading = false;
                if (r) new ManegerAddView().ShowDialog();
            });
        }

        private static bool IsFirstLoad = false;
        public string ServerName { get; set; } = Default.ServerName;

        public string DbName { get; set; } = Default.DbName;

        public bool UseLocalServer { get; set; } = Default.UseLocalServer;

        public ICommand SaveCommand { get; set; }

        public bool CreateDbIfNotExists { get; set; } = Default.CreateDbIfNotExists;
    }
}