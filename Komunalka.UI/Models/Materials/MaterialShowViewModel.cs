﻿using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.Materials
{
    public class MaterialShowViewModel : OneElementBase<Material>
    {
        public MaterialShowViewModel() : base(Repo.Materials)
        {
        }
    }
}