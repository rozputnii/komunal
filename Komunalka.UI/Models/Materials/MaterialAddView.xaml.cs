﻿using Komunalka.DAL;

namespace Komunalka.Models.Materials
{
    /// <summary>
    /// Interaction logic for AddPerson.xaml
    /// </summary>
    public partial class MaterialAddView
    {


        public MaterialAddView(MaterialBase @base)
        {
            InitializeComponent();
            DataContext = new MaterialAddViewModel(@base);
        }

    }
}
