using System;
using System.Collections.ObjectModel;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.Materials
{
    public class MaterialAddViewModel : AddBase<Material>
    {
        public MaterialAddViewModel(MaterialBase mBase) : base(Repo.Materials, () => new Material {MaterialBas = mBase})
        {
            LoadFreeEmployees();
        }

        protected override void ActionMethod(object obj)
        {
            Item.DateInsert=DateTime.Now;
            base.ActionMethod(obj);
        }

        public void LoadFreeEmployees()
        {
            var items = Repo.MaterialBases.GetAll;
            Bases = items?.Convert();
        }

        public ObservableCollection<MaterialBase> Bases { get; set; }
    }
}