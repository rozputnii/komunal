﻿using System;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.Materials
{
    public class MaterialsViewModel : ManyElementBase<Material>
    {
        protected override void Add()
        {
           //new MaterialAddView().Show();
        }

        protected override void Change()
        {
            new MaterialChangeView(SelectedItem).Show();
        }

        protected override async void Remove()
        {
            await Repo.Materials.Delete(SelectedItem);
        }

        public MaterialsViewModel() : base(Repo.Materials)
        {
            //Repo.Employees.Changed += MaterialsOnChanged;
            Repo.Materials.Changed += MaterialsOnChanged;
        }

        private void MaterialsOnChanged(object sender, EventArgs eventArgs)
        {
            base.UpdateItems();
        }
    }
}