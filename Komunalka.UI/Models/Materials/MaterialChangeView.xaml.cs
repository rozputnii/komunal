﻿using Komunalka.DAL;

namespace Komunalka.Models.Materials
{
    /// <summary>
    /// Interaction logic for AddPerson.xaml
    /// </summary>
    public partial class MaterialChangeView
    {
        public MaterialChangeView(Material employee)
        {
            InitializeComponent();
            DataContext = new MaterialsChangeViewModel(employee);
        }

    }
}
