﻿using System.Collections.ObjectModel;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.Materials
{
    public class MaterialsChangeViewModel : ChangeBase<Material>
    {
        public MaterialsChangeViewModel(Material material) : base(Repo.Materials,() => material)
        {
            LoadFreeEmployees();
        }
        public void LoadFreeEmployees()
        {
            var items =  Repo.MaterialBases.GetAll;
            Bases = items?.Convert();
        }

        public ObservableCollection<MaterialBase> Bases { get; set; }

    }
}