﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;
using Komunalka.Models.Specialties;

namespace Komunalka.Models.Employees
{
    public class EmploeesViewModel : ManyElementBase<Employee>
    {
        private readonly DispatcherTimer _updateTimer;
        private bool _allSpecs;
        private bool _needUpdate = true;
        private bool _nowInWork;
        private Schedule _oldSchedule;
        private Specialty _selectedSpecialty;

        public EmploeesViewModel() : base(Repo.Employees)
        { _allSpecs = true;
            FilterCommand2 = new Command(Find);
            ClearCommand = new Command(ClearFilter);
           
            Repo.Specialties.Changed += (sender, args) => { Specialties = Repo.Specialties.GetAll?.Convert(); };
            Repo.Employees.Changed += (sender, args) =>
            {
                UpdateItems();
                SelectedItem = null;
            } ;
            Repo.Schedules.Changed += (sender, args) => { _needUpdate = true; };
            FilterCommand = new Command(ApplyFilter);
            
            _updateTimer = new DispatcherTimer(new TimeSpan(0, 0, 0, 30), DispatcherPriority.ApplicationIdle, Update,
                Dispatcher.CurrentDispatcher);
            _updateTimer?.Stop();
            Specialties = Repo.Specialties.GetAll?.Convert();
        }

        private  void ClearFilter(object o)
        {
            Items = ( Repo.Employees.GetAll)?.Convert();
        }

        private void Find(object o)
        {
            var el = o as string;
            el = el?.ToLower();
            if (el == null) return;

            Items?.Clear();
            if (FindOnId)
            {
                Items =
               (Repo.Employees.GetAll)?.Where(archive => archive?.Phone?.ToString().ToLower().Contains(el)??false)?
                   .Convert();
            }
            else
            {
                Items =
               ( Repo.Employees.GetAll)?.Where(archive => archive?.Name?.ToLower().StartsWith(el) ?? false)?
                   .Convert();
            }

        }
        public ObservableCollection<Specialty> Specialties { get; set; } = new ObservableCollection<Specialty>();

        public bool AllSpecs
        {
            get { return _allSpecs; }
            set
            {
                _allSpecs = value;
                Raise();
                ApplyFilter();
            }
        }

        public ICommand AddSpecialtyCommand { get; set; } = new Command(() => { new AddSpecialtyView().Show(); });

        public Specialty SelectedSpecialty
        {
            get { return _selectedSpecialty; }
            set
            {
                _selectedSpecialty = value;
                SpecialtyIsSelected = value != null;
                Raise();
            }
        }

        public ICommand RemoveSpecialtyCommand { get; set; } =
            new Command(async o => { await Repo.Specialties.Delete(o as Specialty); });

        public ICommand FilterCommand { get; set; }

        public bool SpecialtyIsSelected { get; set; }

        public bool NowInWork
        {
            get { return _nowInWork; }
            set
            {
                _nowInWork = value;
                NowInOnlineUpdate(value);
                Raise();
            }
        }

        private async void Update(object sender, EventArgs eventArgs)
        {
            var currentSchedule = await Repo.Schedules.GetCurrentSchedule();
            if (currentSchedule != null)
            {
                if (currentSchedule.Id != _oldSchedule?.Id)
                {
                    _oldSchedule = currentSchedule;
                    UpdateOne(currentSchedule);
                    _needUpdate = false;
                }
                if (_needUpdate)
                {
                    _needUpdate = false;
                    UpdateOne(currentSchedule);
                }
            }
        }

        private void UpdateOne(Schedule currentSchedule)
        {
            Items = new ObservableCollection<Employee>();
            var empl1 = currentSchedule?.Employees?.Convert();
            var wgs = currentSchedule?.WorkGroups?.Convert();
            if (wgs?.Count > 0)
                foreach (var workGroup in wgs)
                {
                    var empl2 = workGroup?.Employees;
                    if (empl2?.Count > 0)
                    {
                        foreach (var employee in empl2)
                        {
                            Items.Add(employee);
                        }
                    }
                }
            if (empl1?.Count > 0)
            {
                foreach (var employee in empl1)
                {
                    Items.Add(employee);
                }
            }
        }

        

        protected override void Add()
        {
            new EmploeeAddView().Show();
        }

        protected override void Change()
        {
            if (SelectedItem != null)
                new EmploeeChangeView(SelectedItem).Show();
        }

        protected override async void Remove()
        {
            if (SelectedItem != null)
                await Repo.Employees.Delete(SelectedItem);
        }

        

        

        public  void ApplyFilter()
        {
            try
            {
                var items = Repo.Employees.GetAll;

                var enumerable = items as Employee[] ?? items.ToArray();

                if (enumerable.Any())
                {
                    if (AllSpecs)
                    {
                        Items = enumerable.Convert();
                        return;
                    }
                    var newItems = new ObservableCollection<Employee>();
                    foreach (var specialty in Specialties)
                    {
                        foreach (var employee in enumerable)
                        {
                            if (employee.SpecialtyId == specialty.Id && specialty.IsChecked)
                                newItems.Add(employee);
                        }
                    }
                    Items = newItems;
                }
            }
            catch
            {
                
            }
        }

        private void NowInOnlineUpdate(bool v)
        {
            if (v)
            {

                Update(null, null);
                _updateTimer.Start();
            }
            else
            {

                _updateTimer?.Stop();
                UpdateItems();
                _oldSchedule = null;
            }
        }

        

        public async void Filter(bool b)
        {
            if (b)
            {
                var c = await Repo.Schedules.GetCurrentSchedule();
                Items = c?.Employees?.Convert();
            }
            else
            {
                ApplyFilter();
            }
        }

        public string FilterReqtext { get; set; }

        public ICommand FilterCommand2 { get; set; }

        public ICommand ClearCommand { get; set; }

        public bool FindOnId { get; set; }

        public bool FindOnName
        {
            get { return !FindOnId; }
            set { FindOnId = !value; }
        }
    }
}