﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.Employees
{
    public class EmploeeChangeViewModel : ChangeBase<Employee>
    {
        public bool ShowExtra { get; set; }
        public ICommand ExtraCommand { get; set; }
        public EmploeeChangeViewModel(Employee employee) : base(Repo.Employees,() => employee)
        {
            ExtraCommand = new Command(o => { ShowExtra = !ShowExtra; });
            LoadSpecialties();
        }
        public  void LoadSpecialties()
        {
            var items =  Repo.Specialties.GetAll;
            Specialties = items?.Convert();
        }
        public int? Discharge
        {
            get { return Item.Discharge; }
            set
            {
                if (value >= 0)
                    Item.Discharge = value; Raise();
            }
        }
        public int? Experience
        {
            get { return Item.Experience; }
            set
            {
                if (value >= 0)
                    Item.Experience = value; Raise();
            }
        }
        public ObservableCollection<Specialty> Specialties { get; set; }
    }
}
