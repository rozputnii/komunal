﻿using Komunalka.DAL;

namespace Komunalka.Models.Employees
{
    /// <summary>
    /// Interaction logic for AddPerson.xaml
    /// </summary>
    public partial class EmploeeChangeView
    {
        public EmploeeChangeView(Employee employee)
        {
            InitializeComponent();
            DataContext=new EmploeeChangeViewModel(employee);
        }

    }
}
