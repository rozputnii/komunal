﻿using System.Collections.ObjectModel;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.Employees
{
    public class EmploeeAddViewModel: AddBase<Employee>
    {
        public EmploeeAddViewModel() : base(Repo.Employees)
        {
            LoadSpecialties();
        }
        public  void LoadSpecialties()
        {
            var items =  Repo.Specialties.GetAll;
            Specialties = items?.Convert();
        }

        public int? Discharge
        {
            get { return Item.Discharge; }
            set
            {
                if (value >= 0)
                    Item.Discharge=value; Raise();
            }
        }
        public int? Experience
        {
            get { return Item.Experience; }
            set
            {
                if (value >= 0)
                    Item.Experience = value; Raise();
            }
        }

        public ObservableCollection<Specialty> Specialties { get; set; }
    }
}
