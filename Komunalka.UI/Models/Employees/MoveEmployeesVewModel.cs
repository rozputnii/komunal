using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;
using Komunalka.Services;

namespace Komunalka.Models.Employees
{
    public class MoveEmployeesVewModel:MoveItemBaseViewModel<Employee>
    {
        private Specialty _selectedSpecialty;
    
        public ObservableCollection<Specialty> Specialties { get; set; }

        public Specialty SelectedSpecialty
        {
            get { return _selectedSpecialty; }
            set
            {
                _selectedSpecialty = value;
                Filter(value);
                Raise();
            }
        }

        public ObservableCollection<Employee> List { get; set; }
        public void Filter(Specialty specialty)
        {
            new Action(() =>
            {
                if (specialty == null)
                {
                    List.Clear();
                    for (int index = 0; index < SourceCollection?.Count; index++)
                    {
                        var employee = SourceCollection[index];
                        List.Add(employee);
                    }
                }
                else
                {
                    var t = SourceCollection?.Where(employee => employee.Specialty.Id == specialty.Id)?.Convert();
                    List.Clear();
                    foreach (var employee in t)
                    {
                        List.Add(employee);
                    }
                }
            }).RunWithDispatcher();
        }

        public  void Load()
        {
            try
            {
                Specialties = ( Repo.Specialties.GetAll)?.Convert();
            }
            catch
            {
                
            }
        }
        public ObservableCollection<Employee> TemEmployees { get; set; }
        public ObservableCollection<Employee> TemEmployees2 { get; set; }
        protected override void ToDestination(object o)
        {
            base.ToDestination(o);
            List.Remove((Employee) o);
        }

        public void FilterRefresh()
        {
            List.Clear();
            for (int index = 0; index < SourceCollection?.Count; index++)
            {
                var employee = SourceCollection[index];
                List.Add(employee);
            }
        }
        protected override void ToSource(object o)
        {
            base.ToSource(o);
            if(!List.Contains((Employee)o))
            List.Add((Employee)o);
        }

        public ICommand AllCommand { get; set; }

        public MoveEmployeesVewModel(ObservableCollection<Employee> sourceCollection, ObservableCollection<Employee> destinationCollection, Action<Employee> toDestinationCallback, Action<Employee> toSourceCallback) : base(sourceCollection, destinationCollection, toDestinationCallback, toSourceCallback)
        {
            List=new ObservableCollection<Employee>();
            for (int index = 0; index < sourceCollection?.Count; index++)
            {
                var employee = sourceCollection[index];
                List.Add(employee);
            }
            Load();
            AllCommand = new Command(() =>
            {
                SelectedSpecialty = null;
            });
        }

        public MoveEmployeesVewModel(ObservableCollection<Employee> sourceCollection,
            ObservableCollection<Employee> destinationCollection)
            : this(sourceCollection, destinationCollection, null, null)
        {
            
        }
    }
}