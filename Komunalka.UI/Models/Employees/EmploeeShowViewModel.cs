﻿using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.Employees
{
    public class EmploeeShowViewModel : OneElementBase<Employee>
    {
        public EmploeeShowViewModel() : base(Repo.Employees)
        {
        }
    }
}
