﻿using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.MaterialBases
{
    public class MaterialBaseShowViewModel : OneElementBase<MaterialBase>
    {
        public MaterialBaseShowViewModel() : base(Repo.MaterialBases)
        {
        }
    }
}