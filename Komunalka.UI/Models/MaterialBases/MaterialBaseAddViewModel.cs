﻿using System.Collections.ObjectModel;
using System.Linq;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.MaterialBases
{
    public class MaterialBaseAddViewModel : AddBase<MaterialBase>
    {
        public MaterialBaseAddViewModel() : base(Repo.MaterialBases)
        {
            LoadFreeEmployees();
        }

        public async void LoadFreeEmployees()
        {
            Specialties = ( Repo.Specialties.GetAll)?.Convert();
            if (Specialties?.Count > 0)
            {
                SelectedSpecialty = Specialties?[0];
                if (SelectedSpecialty?.Employees?.Count > 0)
                {
                    if(Item!=null)
                    Item.Employee = SelectedSpecialty.Employees.ElementAt(0);
                }
            }
        }

        public Specialty SelectedSpecialty { get; set; }
        public ObservableCollection<Specialty> Specialties { get; set; }
    }
}