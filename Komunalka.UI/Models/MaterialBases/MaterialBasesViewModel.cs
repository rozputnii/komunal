﻿using System;
using System.Windows.Input;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;
using Komunalka.Models.Materials;

namespace Komunalka.Models.MaterialBases
{
    public class MaterialBasesViewModel : ManyElementBase<MaterialBase>
    {
        private Material _selectedMaterial;
        protected override void Add()
        {
            new MaterialBaseAddView().Show();
        }

        protected override void Change()
        {
            if (SelectedItem != null)
                new MaterialBaseChangeView(SelectedItem).Show();
        }

        protected override async void Remove()
        {
            if (SelectedItem != null)
                await Repo.MaterialBases.Delete(SelectedItem);
        }

        public MaterialBasesViewModel() : base(Repo.MaterialBases)
        {
            Repo.Employees.Changed += MaterialBasesOnChanged;
            Repo.MaterialBases.Changed += MaterialBasesOnChanged;
            Repo.Materials.Changed += (sender, args) => { SelectedItem = null; };
        }
       

        public ICommand MatAddCommand { get; set; } =new Command(o =>
        {
            var ob = o as MaterialBase;
            if (ob != null)
                new MaterialAddView(ob).Show();
        });
        public ICommand MatChangeCommand { get; set; } = new Command(o =>
        {
            var ob = o as Material;
            if (ob != null)
                new MaterialChangeView(ob).Show();
        });
        public ICommand MatRemoveCommand { get; set; } = new Command(async o =>
        {
            var ob = o as Material;
            if(ob!=null)
                await Repo.Materials.Delete(ob);
        });

        public Material SelectedMaterial
        {
            get { return _selectedMaterial; }
            set
            {
                _selectedMaterial = value;
                MaterialIsSelected = value != null;
                Raise();
            }
        }

        public bool MaterialIsSelected { get; set; }
        private void MaterialBasesOnChanged(object sender, EventArgs eventArgs)
        {
            base.UpdateItems();
        }
    }
}