﻿using Komunalka.DAL;

namespace Komunalka.Models.MaterialBases
{
    /// <summary>
    /// Interaction logic for AddPerson.xaml
    /// </summary>
    public partial class MaterialBaseChangeView
    {
        public MaterialBaseChangeView(MaterialBase employee)
        {
            InitializeComponent();
            DataContext = new MaterialBaseChangeViewModel(employee);
        }

    }
}
