﻿using System.Collections.ObjectModel;
using System.Linq;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.MaterialBases
{
    public class MaterialBaseChangeViewModel : ChangeBase<MaterialBase>
    {
        public MaterialBaseChangeViewModel(MaterialBase materialBase) : base(Repo.MaterialBases,() => materialBase)
        {
            LoadFreeEmployees(materialBase);
        }
        public  void LoadFreeEmployees(MaterialBase materialBase)
        {
            Specialties = ( Repo.Specialties.GetAll).Convert();

            if (Specialties != null)
            {
                SelectedSpecialty = Specialties?.FirstOrDefault(specialty => specialty.Id == materialBase?.Employee?.Specialty?.Id);
            }
            materialBase.Employee =
                SelectedSpecialty?.Employees?.FirstOrDefault(employee => employee.Id == materialBase.Employee?.Id);
        }
        public Specialty SelectedSpecialty { get; set; }
        public ObservableCollection<Specialty> Specialties { get; set; }

    }
}