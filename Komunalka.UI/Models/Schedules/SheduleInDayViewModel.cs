﻿using System;
using Komunalka.DAL;
using Komunalka.Models.Base;

namespace Komunalka.Models.Schedules
{
    public class SheduleInDay:ViewModelBase
    {
        public Schedule Schedule1 { get; set; }
        public Schedule Schedule2 { get; set; }
        public Schedule Schedule3 { get; set; }

        public SheduleInDay(DateTime date, Schedule schedule1, Schedule schedule2, Schedule schedule3)
        {
            Date = date;
            Schedule1 = schedule1;
            Schedule2 = schedule2;
            Schedule3 = schedule3;
        }

        public  DateTime Date { get; set; }
    }
}
