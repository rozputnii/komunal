using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;
using Komunalka.Models.Employees;
using Komunalka.Models.WorkGroups;

namespace Komunalka.Models.Schedules
{
    public class ScheduleAddViewModel : AddBase<Schedule>
    {
        public ScheduleAddViewModel() : base(Repo.Schedules)
        {
            Load();
        }

        public ObservableCollection<WorkGroup> WorkGroups { get; set; }
        public ObservableCollection<Employee> Employees { get; set; }

        public DateTime DateStart { get; set; } = DateTime.Today;
        public DateTime DateEnd { get; set; } = DateTime.Today;

        public MoveWorkGroupsVewModel MoveWorkGroup1 { get; set; }

        public MoveEmployeesVewModel MoveEmployees1 { get; set; }



        public MoveWorkGroupsVewModel MoveWorkGroup2 { get; set; }
        public MoveEmployeesVewModel MoveEmployees2 { get; set; }
        public MoveWorkGroupsVewModel MoveWorkGroup3 { get; set; }
        public MoveEmployeesVewModel MoveEmployees3 { get; set; }

        public void Load()
        {
            try
            {
                WorkGroups = ( Repo.WorkGroups.GetAll)?.Convert();
                Employees = ( Repo.Employees.GetAll)?.Where(employee => employee.WorkGroup == null)?.Convert();
                MoveWorkGroup1 = new MoveWorkGroupsVewModel(WorkGroups, null, ToDestWorkGroup1, ToSourceWorkGroup1);
                MoveEmployees1 = new MoveEmployeesVewModel(Employees, null, ToDestEmployees1, ToSourceEmployees1);
                MoveWorkGroup2 = new MoveWorkGroupsVewModel(WorkGroups, null, ToDestWorkGroup2, ToSourceWorkGroup2);
                MoveEmployees2 = new MoveEmployeesVewModel(Employees, null, ToDestEmployees2, ToSourceEmployees2);
                MoveWorkGroup3 = new MoveWorkGroupsVewModel(WorkGroups, null, ToDestWorkGroup3, ToSourceWorkGroup3);
                MoveEmployees3 = new MoveEmployeesVewModel(Employees, null, ToDestEmployees3, ToSourceEmployees3);
            }
            catch
            {
            }
        }

        protected override async void ActionMethod(object obj)
        {
            var date = DateStart;
            while (date <= DateEnd)
            {
                var itsss = ( Repo.Schedules.GetAll)?.Where(schedule => schedule.Date.Date == date.Date);

                var item1 = new Schedule
                {
                    Date = date,
                    Employees = MoveEmployees1.DestinationCollection,
                    WorkGroups = MoveWorkGroup1.DestinationCollection,
                    Kind = 0,
                    Name = "���� 1"
                };
                var arr = itsss as Schedule[] ?? itsss?.ToArray();
                var r1 = arr?.FirstOrDefault(schedule => schedule.Kind == 0);
                if(r1==null)
                await Repository.Add(item1);
                var item2 = new Schedule
                {
                    Date = date,
                    Employees = MoveEmployees2.DestinationCollection,
                    WorkGroups = MoveWorkGroup2.DestinationCollection,
                    Kind = 1,
                    Name = "���� 2"
                };
                var r2 = arr?.FirstOrDefault(schedule => schedule.Kind == 1);
                if (r2 == null)
                    await Repository.Add(item2);
                var item3 = new Schedule
                {
                    Date = date,
                    Employees = MoveEmployees3.DestinationCollection,
                    WorkGroups = MoveWorkGroup3.DestinationCollection,
                    Kind = 2,
                    Name = "���� 3"
                };
                var r3 = arr?.FirstOrDefault(schedule => schedule.Kind == 2);
                if (r3 == null)
                    await Repository.Add(item3);
                date = date.AddDays(1);
            }
            (obj as Window)?.Close();
        }

        public void Remove(Employee employee)
        {
            //Employees.Remove(employee);
        }

        public void Add(Employee employee)
        {
            //Employees.Add(employee);
        }

        public void ToDestWorkGroup1(WorkGroup workGroup)
        {
           
        }

        public void ToDestEmployees1(Employee employee)
        {
            Remove(employee);
            MoveEmployees2.FilterRefresh();
            MoveEmployees3.FilterRefresh();
        }
        
        public void ToSourceWorkGroup1(WorkGroup workGroup)
        {
        }

        public void ToSourceEmployees1(Employee employee)
        {
            Add(employee);
            MoveEmployees2.FilterRefresh();
            MoveEmployees3.FilterRefresh();
        }

        public void ToDestWorkGroup2(WorkGroup workGroup)
        {
        }

        public void ToDestEmployees2(Employee employee)
        {
            Remove(employee);
            MoveEmployees1.FilterRefresh();
            MoveEmployees3.FilterRefresh();
        }

        public void ToSourceWorkGroup2(WorkGroup workGroup)
        {
        }

        public void ToSourceEmployees2(Employee employee)
        {
            Add(employee);
            MoveEmployees1.FilterRefresh();
            MoveEmployees3.FilterRefresh();
        }

        public void ToDestWorkGroup3(WorkGroup workGroup)
        {
          
        }

        public void ToDestEmployees3(Employee employee)
        {
            Remove(employee);
            MoveEmployees1.FilterRefresh();
            MoveEmployees2.FilterRefresh();
        }

        public void ToSourceWorkGroup3(WorkGroup workGroup)
        {
        }

        public void ToSourceEmployees3(Employee employee)
        {
            Add(employee);
            MoveEmployees1.FilterRefresh();
            MoveEmployees2.FilterRefresh();
        }
    }
}