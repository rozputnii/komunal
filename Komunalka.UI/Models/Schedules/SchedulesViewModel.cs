using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using GemBox.Spreadsheet;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;
using Komunalka.Services;
using SaveFileDialog = Microsoft.Win32.SaveFileDialog;

namespace Komunalka.Models.Schedules
{
    public class SchedulesViewModel : ManyElementBase<Schedule>
    {
        private Schedule _selectedItem1;

        protected override void Add()
        {
            new ScheduleAddView().Show();
        }

        protected override async void Remove()
        {
            if (SelectedItem != null)
               await Repo.Schedules.Delete(SelectedItem);
        }
        
        public override Schedule SelectedItem
        {
            get { return _selectedItem1; }
            set
            {
                _selectedItem1 = value;
                ItemSelected = SelectedItem?.TimeStart() > DateTime.Now;
                Raise();
            }
        }

        protected override void Change()
        {
            if (SelectedItem != null)
                new ScheduleChangeView(SelectedItem).Show();
        }

        protected override void Show()
        {}

        public SchedulesViewModel() : base(Repo.Schedules)
        {
            RemoveArhiveCommand=new Command(RemoveArhive);
            ToWordCommand = new Command(ToWordWorkGroups);
            ToWordEmploeesCommand = new Command(ToWordWorkEmpl);
            Repo.Schedules.Changed += (sender, args) =>
            {
                UpdateItems();
                SelectedItem = null;
            };
            ShowOnWorkGroupsCommand=new Command(ShowOnWorkGropus);
        }

        private async void RemoveArhive()
        {
            try
            {
                DialogResult dialogResult = MessageBox.Show("�������� �� ������ ���� � ������?", "��������� ������� ���", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    if (Items1?.Count > 0)
                    {
                        foreach (var schedule in Items1)
                        {
                            await Repo.Schedules.Delete(schedule);
                        }
                    }
                    //do something
                }
            }
            catch
            {

            }
        }

        protected override void UpdateItems()
        {
            try
            {
                var items = (Repo.Schedules.GetAll)?.OrderBy(schedule => schedule.Date).Reverse();
                var schedules = items as Schedule[] ?? items?.ToArray();
                Items = schedules?.Where(schedule => schedule.Date.Date >= DateTime.Now.Date)?.Convert();
                Items1 = schedules?.Where(schedule => schedule.Date.Date < DateTime.Now.Date)?.Convert();
            }
            catch
            {
            }
        }

        private  void ShowOnWorkGropus()
        {
            var all =  Repo.Schedules.GetAll;
            var nall = all.Where(schedule => schedule.Date >= DateStart && schedule.Date <= DateEnd);
            SheduleInDays = new ObservableCollection<SheduleInDay>();
            PasteWGrs(nall, SheduleInDays);
        }

        public void PasteWGrs(IEnumerable<Schedule> schedules, ObservableCollection<SheduleInDay> sheduleInDays)
        {
            new Action(() =>
            {
                var enumerable = schedules as Schedule[] ?? schedules.ToArray();
                if (enumerable.Any())
                {
                    for (var time = DateStart; time <= DateEnd; time = time.AddDays(1))
                    {
                        var newScheds = enumerable.Where(schedule => schedule.Date == time);
                        var scheds = newScheds as Schedule[] ?? newScheds.ToArray();
                        var s1 = scheds?.FirstOrDefault(schedule => schedule.Kind == 0);
                        var s2 = scheds?.FirstOrDefault(schedule => schedule.Kind == 1);
                        var s3 = scheds?.FirstOrDefault(schedule => schedule.Kind == 2);
                        sheduleInDays.Add(new SheduleInDay(time, s1, s2, s3));
                    }
                }
            }).RunWithDispatcher();

        }

        public  ObservableCollection<SheduleInDay> SheduleInDays { get; set; }
        public DateTime DateStart { get; set; } = DateTime.Today;
        public DateTime DateEnd { get; set; } = DateTime.Today.AddMonths(1);

        public ICommand ShowOnWorkGroupsCommand { get; set; }

        public ObservableCollection<Schedule> Items1 { get; set; }

        public ICommand ToWordCommand { get; set; }
        public ICommand ToWordEmploeesCommand { get; set; }

        public ICommand RemoveArhiveCommand { get; set; }

        private void ToWordWorkGroups()
        {
            try
            {
                var dlg = new SaveFileDialog
                {
                    Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*",
                    RestoreDirectory = true
                };
                dlg.ShowDialog();
                var path = dlg.FileName;
                if (File.Exists(path))
                    File.Delete(path);
                ToWordWorkGroups(path);
            }
            catch
            {
            }
        }
        private void ToWordWorkEmpl()
        {
            try
            {
                var dlg = new SaveFileDialog
                {
                    Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*",
                    RestoreDirectory = true
                };
                dlg.ShowDialog();
                var path = dlg.FileName;
                if (File.Exists(path))
                    File.Delete(path);
                ToWordEmploees(path);
            }
            catch
            {
            }
        }


        private void ToWordWorkGroups(string path)
        {
            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");
            ExcelFile ef = new ExcelFile();
            ExcelWorksheet ws = ef.Worksheets.Add("����������");
            ws.Cells[0, 0].Value = "����";
            ws.Cells[0, 1].Value = "���� 1";
            ws.Cells[0, 2].Value = "���� 2";
            ws.Cells[0, 3].Value = "���� 3";

            ws.Rows[0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Columns[0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Columns[0].Width = (int)LengthUnitConverter.Convert(4, LengthUnit.Centimeter, LengthUnit.ZeroCharacterWidth256thPart);

            ws.Columns[1].Width =
               ws.Columns[2].Width =
               ws.Columns[3].Width =
                       (int)LengthUnitConverter.Convert(6, LengthUnit.Centimeter, LengthUnit.ZeroCharacterWidth256thPart);

            var currentRow = 1;
            if(SheduleInDays?.Count>0)
                foreach (var scedule in SheduleInDays)
                {
                    PasteInCurrentRow(scedule, ref currentRow, ref ws);
                }
            ef.Save(path);
            var cmd = "explorer.exe";
            var arg = "/select, " + path;
            Process.Start(cmd, arg);
        }

        private void PasteInCurrentRow(SheduleInDay schedule, ref int row,ref ExcelWorksheet ws)
        {
            var wg1 = schedule.Schedule1?.WorkGroups;
            var wg2 = schedule.Schedule2?.WorkGroups;
            var wg3 = schedule.Schedule3?.WorkGroups;

            var totalCount = wg1?.Count??0;

            if (wg2?.Count > totalCount)
                totalCount = wg2.Count;
            if (wg3?.Count > totalCount)
                totalCount = wg3.Count;
            var i = 0;
            ws.Cells[row, 0].Value = schedule.Date.ToString("dd.MM.yy");
            if (wg1?.Count>0)
                foreach (var wg in wg1)
                {
                    ws.Cells[row + i, 1].Value = wg.Name;
                    i++;
                }
            i = 0;
            if (wg2?.Count > 0)
                foreach (var wg in wg2)
                {
                    ws.Cells[row + i, 2].Value = wg.Name;
                    i++;
                }
            i = 0;
            if (wg3?.Count > 0)
                foreach (var wg in wg3)
                {
                    ws.Cells[row + i, 3].Value = wg.Name;
                    i++;
                }
            row += totalCount+1;
        }

        private void ToWordEmploees(string path)
        {
            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");
            ExcelFile ef = new ExcelFile();
            ExcelWorksheet ws = ef.Worksheets.Add("����������");
            ws.Cells[0, 0].Value = "����";
            ws.Cells[0, 1].Value = "���� 1";
            ws.Cells[0, 2].Value = "���� 2";
            ws.Cells[0, 3].Value = "���� 3";

            ws.Rows[0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Columns[0].Style.Font.Weight = ExcelFont.BoldWeight;
            ws.Columns[0].Width = (int)LengthUnitConverter.Convert(4, LengthUnit.Centimeter, LengthUnit.ZeroCharacterWidth256thPart);

            ws.Columns[1].Width =
               ws.Columns[2].Width =
               ws.Columns[3].Width =
                       (int)LengthUnitConverter.Convert(6, LengthUnit.Centimeter, LengthUnit.ZeroCharacterWidth256thPart);

            var currentRow = 1;
            if (SheduleInDays?.Count > 0)
                foreach (var scedule in SheduleInDays)
                {
                    PasteInCurrentRowEmploees(scedule, ref currentRow, ref ws);
                }
            ef.Save(path);
            var cmd = "explorer.exe";
            var arg = "/select, " + path;
            Process.Start(cmd, arg);
        }

        private void PasteInCurrentRowEmploees(SheduleInDay schedule, ref int row, ref ExcelWorksheet ws)
        {
            
            var wg1 = schedule.Schedule1?.Employees;
            var wg2 = schedule.Schedule2?.Employees;
            var wg3 = schedule.Schedule3?.Employees;

            var totalCount = wg1?.Count ?? 0;

            if (wg2?.Count > totalCount)
                totalCount = wg2.Count;
            if (wg3?.Count > totalCount)
                totalCount = wg3.Count;
            
            var i = 0;
            ws.Cells[row, 0].Value = schedule.Date.ToString("dd.MM.yy");
            if (wg1?.Count > 0)
                foreach (var wg in wg1)
                {
                    ws.Cells[row + i, 1].Value = wg.Name;
                    i++;
                }
            i = 0;
            if (wg2?.Count > 0)
                foreach (var wg in wg2)
                {
                    ws.Cells[row + i, 2].Value = wg.Name;
                    i++;
                }
            i = 0;
            if (wg3?.Count > 0)
                foreach (var wg in wg3)
                {
                    ws.Cells[row + i, 3].Value = wg.Name;
                    i++;
                }

            row += totalCount + 1;
        }

    }
}