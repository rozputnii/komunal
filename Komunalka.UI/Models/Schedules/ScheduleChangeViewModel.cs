using System.Collections.ObjectModel;
using System.Linq;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;
using Komunalka.Models.Employees;
using Komunalka.Models.WorkGroups;

namespace Komunalka.Models.Schedules
{
    public class ScheduleChangeViewModel : ChangeBase<Schedule>
    {
        public ScheduleChangeViewModel(Schedule schedule) : base(Repo.Schedules, () => schedule)
        {
            Load(schedule);
        }

        public void Load(Schedule schedule)
        {
            try
            {
                WorkGroups=new ObservableCollection<WorkGroup>();
                var ws = Repo.WorkGroups.GetAll?.Convert();
                if (ws != null)
                {
                    foreach (var workGroup in ws)
                    {
                        bool exists = workGroup.Schedules.Any(schedule1 => schedule1.Date == schedule.Date);
                        if(!exists)
                        WorkGroups.Add(workGroup);
                    }
                }
                Employees=new ObservableCollection<Employee>();
                var es = Repo.Employees.GetAll?.Where(employee => employee.WorkGroup == null)?.Convert();
                if (es != null)
                {
                    foreach (var emploee in es)
                    {
                        bool exists = emploee.Schedules.Any(schedule1 => schedule1.Date == schedule.Date);
                        if (!exists)
                            Employees.Add(emploee);
                    }
                }

               
                MoveWorkGroup1 = new MoveWorkGroupsVewModel(WorkGroups, schedule.WorkGroups?.Convert(), null, null);
                MoveEmployees1 = new MoveEmployeesVewModel(Employees, schedule.Employees?.Convert(), null, null);
            }
            catch
            {
            }
        }

        public ObservableCollection<WorkGroup> WorkGroups { get; set; }
        public ObservableCollection<Employee> Employees { get; set; }
        public MoveWorkGroupsVewModel MoveWorkGroup1 { get; set; }

        public MoveEmployeesVewModel MoveEmployees1 { get; set; }
        protected override void ActionMethod(object obj)
        {
            Item.Employees = MoveEmployees1.DestinationCollection;
            Item.WorkGroups = MoveWorkGroup1.DestinationCollection;
            base.ActionMethod(obj);
        }
    }
}