﻿using Komunalka.DAL;

namespace Komunalka.Models.Schedules
{
    /// <summary>
    /// Interaction logic for AddPerson.xaml
    /// </summary>
    public partial class ScheduleChangeView
    {
        public ScheduleChangeView(Schedule el)
        {
            InitializeComponent();
            DataContext = new ScheduleChangeViewModel(el);
        }

    }
}
