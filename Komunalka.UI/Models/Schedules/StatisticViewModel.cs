using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;
using System.Windows.Input;
using GemBox.Document;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using GemBox.Spreadsheet;
using GemBox.Spreadsheet.Charts;
using Microsoft.Win32;
using LengthUnit = GemBox.Spreadsheet.LengthUnit;
using LengthUnitConverter = GemBox.Spreadsheet.LengthUnitConverter;

namespace Komunalka.Models.Schedules
{
    public class Item
    {
        public string Label { get; set; }
        public int Value1 { get; set; }
        public int Value2 { get; set; }
        public int Value3 { get; set; }

        //public double Value3 { get; set; }
        //public double Value2 { get; set; }
    }

    public class StatisticViewModel : ViewModelBase
    {
        public StatisticViewModel()
        {
            ToPdfCommand=new Command(SetPdf);
            ToExcelCommand = new Command(SetToXls);
            GetCommand = new Command(Get);
            LoadKindOfRequests();
        }

        private void SetPdf()
        {
            Task.Run(() =>
            {
                try
                {
                    var dlg = new SaveFileDialog();
                    dlg.Filter = "Pdf files (*.pdf)|*.pdf|All files (*.*)|*.*";
                    dlg.RestoreDirectory = true;
                    dlg.ShowDialog();
                    var path = dlg.FileName;
                    if (File.Exists(path))
                        File.Delete(path);
                    // If using Professional version, put your serial key below.
                    using (var stream = File.Create(path))
                    {
                        var pdfExporter = new PdfExporter { Width = 640, Height = 480 };
                        pdfExporter.Export(Model1, stream);
                    }
                    var cmd = "explorer.exe";
                    var arg = "/select, " + path;
                    Process.Start(cmd, arg);
                }
                catch (Exception exception)
                {

                }
            });
        }

        public DateTime DateStart { get; set; } = DateTime.Today.AddMonths(-1);
        public DateTime DateEnd { get; set; } = DateTime.Today;

        public ICommand GetCommand { get; set; }
        //public SeriesCollection SeriesCollection { get; set; } = new SeriesCollection();

        public PlotModel Model1 { get; set; }

        public int RangeIndex { get; set; } = 0;
        private Collection<Item> ITEMS;
        private void Get()
        {
            try
            {
                IEnumerable<Request> all;
                if (SelectedKindOfRequest != null &&!AllReq)
                {
                    all = (Repo.Requests.All)?.Where(request => request.KindOfRequest == SelectedKindOfRequest);
                }
                else
                {
                    all = (Repo.Requests.All);
                }

                var requests = all?.ToArray();

                if (requests?.Any() ?? false)
                {
                    //Model1 = tmp;
                    var items = new Collection<Item>();
                    
                    if (RangeIndex == 1)
                    {
                        OnWeeks(ref items, requests);
                    }
                    else if (RangeIndex == 2)
                    {
                        OnMoonth(ref items, requests);
                    }
                    else
                    {
                        OnDays(ref items, requests);
                    }
                    if (SelectedKindOfRequest == null || AllReq)
                    {
                        Model1 = GenerateModel(items, "������ ��� ����");
                    }
                    else
                    {
                        Model1 = GenerateModel(items, "������: "+SelectedKindOfRequest.Name);
                    }
                    ITEMS = items;
                }
            }
            catch (Exception exception)
            {
            }
        }
        private void OnDays(ref Collection<Item> items, Request[] requests)
        {
            for (var time = DateStart; time <= DateEnd; time = time.AddDays(1))
            {
                var it = new Item
                {
                    Label = time.ToString("dd.MM"),
                    Value1 = requests.Count(request => request.Date.Date == time.Date && request.Status == 0),
                    Value2 = requests.Count(request => request.Date.Date == time.Date && request.Status == 1),
                    Value3 = requests.Count(request => request.Date.Date == time.Date && request.Status == 2)
                };
                items.Add(it);
            }
        }
        private void OnWeeks(ref Collection<Item> items, Request[] requests)
        {
            for (var time = DateStart; time <= DateEnd; time = time.AddDays(7))
            {
                var t1 = time.AddDays(7);
                
                var it = new Item
                {
                    Label = time.ToString("dd.MM")+" - "+ t1.ToString("dd.MM"),
                    Value1 = requests.Count(request => request.Date >= time.Date && request.Date<t1.Date && request.Status==0),
                    Value2 = requests.Count(request => request.Date >= time.Date && request.Date < t1.Date && request.Status == 1),
                    Value3 = requests.Count(request => request.Date >= time.Date && request.Date < t1.Date && request.Status == 2)
                };
                items.Add(it);
            }
        }
        private void OnMoonth(ref Collection<Item> items, Request[] requests)
        {
            for (var time = DateStart; time <= DateEnd; time = time.AddMonths(1))
            {
                var it = new Item
                {
                    Label = time.ToString("MMMM"),
                    Value1 = requests.Count(request => request.Date.Month == time.Date.Month && request.Status == 0),
                    Value2 = requests.Count(request => request.Date.Month == time.Date.Month && request.Status == 1),
                    Value3 = requests.Count(request => request.Date.Month == time.Date.Month && request.Status == 2)
                };
                items.Add(it);
            }
        }
        private PlotModel GenerateModel(Collection<Item> items, string name)
        {
            var tmp = new PlotModel
            {
                Title = name,
                LegendPlacement = LegendPlacement.Outside,
                LegendPosition = LegendPosition.RightTop,
                LegendOrientation = LegendOrientation.Vertical,
               };

            // Add the axes, note that MinimumPadding and AbsoluteMinimum should be set on the value axis.
            tmp.Axes.Add(new OxyPlot.Axes.CategoryAxis { ItemsSource = items, LabelField = nameof(Item.Label) });
            tmp.Axes.Add(new LinearAxis {Position = AxisPosition.Left, MinimumPadding = 0, AbsoluteMinimum = 0});

            // Add the series, note that the BarSeries are using the same ItemsSource as the CategoryAxis.
            if(ForDo)
            { tmp.Series.Add(new ColumnSeries {Title = "����� ���������", ItemsSource = items, ValueField = nameof(Item.Value1)});}
            if (InDo)
                tmp.Series.Add(new ColumnSeries { Title = "����������", ItemsSource = items, ValueField = nameof(Item.Value2) });
            if (IsDone)
                tmp.Series.Add(new ColumnSeries { Title = "��������", ItemsSource = items, ValueField = nameof(Item.Value3) });
            return tmp;
        }

        public void LoadKindOfRequests()
        {
            KindOfRequests = Repo.KindOfRequests.GetAll?.Convert();
            
        }
        public ICommand ToExcelCommand { get; set; }
        public void SetToXls()
        {
            Task.Run(() =>
            {
                try
                {
                    var dlg = new SaveFileDialog();
                    dlg.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                    dlg.RestoreDirectory = true;
                    dlg.ShowDialog();
                    var path = dlg.FileName;
                    if (File.Exists(path))
                        File.Delete(path);
                    // If using Professional version, put your serial key below.
                    SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");
                    ExcelFile ef = new ExcelFile();
                    ExcelWorksheet ws = ef.Worksheets.Add("����������");

                    int numberItems = ITEMS.Count;


                    // Create chart and select data for it.
                    var chart = ws.Charts.Add(ChartType.Bar, "E2", "M25");
                    int maxColumns = 0;
                    if (ForDo) maxColumns++;
                    if (InDo) maxColumns++;
                    if (IsDone) maxColumns++;
                    chart.SelectData(ws.Cells.GetSubrangeAbsolute(0, 0, numberItems, maxColumns), true);

                    // Add data which is used by the chart.


                    for (int i = 0; i < numberItems; i++)
                    {
                        var one = ITEMS[i];
                        ws.Cells[i + 1, 0].Value = one.Label;
                        ws.Cells[i + 1, 1].Value = one.Value1;
                        ws.Cells[i + 1, 2].Value = one.Value2;
                        ws.Cells[i + 1, 3].Value = one.Value3;

                    }

                    // Set header row and formatting.
                    ws.Cells[0, 0].Value = "����";
                    ws.Cells[0, 1].Value = "����� ���������";
                    ws.Cells[0, 2].Value = "����������";
                    ws.Cells[0, 3].Value = "��������";
                    ws.Cells[0, 0].Style.Font.Weight =
                        ws.Cells[0, 1].Style.Font.Weight =
                            ws.Cells[0, 2].Style.Font.Weight =
                                ws.Cells[0, 3].Style.Font.Weight = ExcelFont.BoldWeight;

                    ws.Columns[0].Width =
                        (int)
                            LengthUnitConverter.Convert(3, LengthUnit.Centimeter, LengthUnit.ZeroCharacterWidth256thPart);

                    // Make entire sheet print on a single page.
                    ws.PrintOptions.FitWorksheetWidthToPages = ws.PrintOptions.FitWorksheetHeightToPages = 1;

                    ef.Save(path);
                    var cmd = "explorer.exe";
                    var arg = "/select, " + path;
                    Process.Start(cmd, arg);
                }
                catch (Exception exception)
                {

                }
            });
        }


        public ObservableCollection<KindOfRequest> KindOfRequests { get; set; }

        public KindOfRequest SelectedKindOfRequest { get; set; }

        public bool AllReq { get; set; } = true;

        public bool ForDo { get; set; } = true;

        public bool InDo { get; set; } = true;

        public bool IsDone { get; set; } = true;

        public ICommand ToPdfCommand { get; set; }
    }
}