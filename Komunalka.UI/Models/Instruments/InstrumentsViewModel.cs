﻿using System;
using System.Linq;
using System.Windows.Input;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.Instruments
{
    public class InstrumentsViewModel : ManyElementBase<Instrument>
    {
        protected override void Add()
        {
           new InstrumentAddView().Show();
        }

        protected override void Change()
        {
            new InstrumentChangeView(SelectedItem).Show();
        }

        protected override async void Remove()
        {
            await Repo.Instruments.Delete(SelectedItem);
        }

        public InstrumentsViewModel() : base(Repo.Instruments)
        {
            Repo.Instruments.Changed +=(sender, args) => 
            {
                base.UpdateItems();
                SelectedItem = null;
            };
            FilterCommand = new Command(Find);
            ClearCommand = new Command(ClearFilter);
        }
        private  void ClearFilter(object o)
        {
            Items = ( Repo.Instruments.GetAll)?.Convert();
        }

        private  void Find(object o)
        {
            var el = o as string;
            el = el?.ToLower();
            if (el == null) return;

            Items?.Clear();
            if (FindOnId)
            {
                Items =
               ( Repo.Instruments.GetAll)?.Where(archive => archive.Id.ToString().ToLower().StartsWith(el))?
                   .Convert();
            }
            else
            {
                Items =
               ( Repo.Instruments.GetAll)?.Where(archive => archive.Name?.ToLower().StartsWith(el) ?? false)?
                   .Convert();
            }

        }
       

        public string FilterReqtext { get; set; }

        public ICommand FilterCommand { get; set; }

        public ICommand ClearCommand { get; set; }

        public bool FindOnId { get; set; }

        public bool FindOnName
        {
            get { return !FindOnId; }
            set { FindOnId = !value; }
        }
    }
}