using System;
using System.Collections.ObjectModel;
using System.Linq;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.Instruments
{
    public class InstrumentAddViewModel : AddBase<Instrument>
    {
        public InstrumentAddViewModel() : base(Repo.Instruments)
        {
            LoadFreeEmployees();
        }

        protected override void ActionMethod(object obj)
        {
            Item.DateInsert=DateTime.Now;
            base.ActionMethod(obj);
        }

        public  void LoadFreeEmployees()
        {
            Specialties = ( Repo.Specialties.GetAll)?.Convert();
            if (Specialties?.Count > 0)
            {
                SelectedSpecialty = Specialties?[0];
                if (SelectedSpecialty?.Employees?.Count > 0)
                {
                    if (Item != null)
                        Item.Employee = SelectedSpecialty.Employees.ElementAt(0);
                }
            }
        }
        public Specialty SelectedSpecialty { get; set; }
        public ObservableCollection<Specialty> Specialties { get; set; }
    }
}