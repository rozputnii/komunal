﻿using System.Collections.ObjectModel;
using System.Linq;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.Instruments
{
    public class InstrumentChangeViewModel : ChangeBase<Instrument>
    {

        public InstrumentChangeViewModel(Instrument material) : base(Repo.Instruments,() => material)
        {
            LoadFreeEmployees(material);
        }
       
        public  void LoadFreeEmployees(Instrument instrument)
        {
            Specialties = ( Repo.Specialties.GetAll).Convert();

            if (Specialties != null)
            {
                SelectedSpecialty = Specialties?.FirstOrDefault(specialty => specialty.Id == instrument?.Employee?.Specialty?.Id);
            }
            instrument.Employee =
                SelectedSpecialty?.Employees?.FirstOrDefault(employee => employee.Id == instrument.Employee?.Id);
        }
        public Specialty SelectedSpecialty { get; set; }
        public ObservableCollection<Specialty> Specialties { get; set; }

    }
}