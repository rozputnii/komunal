﻿using Komunalka.DAL;

namespace Komunalka.Models.Instruments
{
    /// <summary>
    /// Interaction logic for AddPerson.xaml
    /// </summary>
    public partial class InstrumentChangeView
    {
        public InstrumentChangeView(Instrument employee)
        {
            InitializeComponent();
            DataContext = new InstrumentChangeViewModel(employee);
        }

    }
}
