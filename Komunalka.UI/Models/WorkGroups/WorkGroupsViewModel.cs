﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.WorkGroups
{
    public class WorkGroupsViewModel : ManyElementBase<WorkGroup>
    {
        private readonly DispatcherTimer _updateTimer;
        private bool _needUpdate = true;
        private bool _nowInWork;

        private Schedule _oldSchedule;

        public WorkGroupsViewModel() : base(Repo.WorkGroups)
        {
            Repo.WorkGroups.Changed += (sender, args) =>
            {
                UpdateItems();
                SelectedItem = null;
            };
            //Repo.Employees.Changed += MaterialBasesOnChanged;
            _updateTimer = new DispatcherTimer(new TimeSpan(0, 0, 0, 30), DispatcherPriority.ApplicationIdle, Update,
                Dispatcher.CurrentDispatcher);
            _updateTimer?.Stop();
        }

        public bool NowInWork
        {
            get { return _nowInWork; }
            set
            {
                _nowInWork = value;
                NowInOnlineUpdate(value);
                Raise();
            }
        }

        private void NowInOnlineUpdate(bool v)
        {
            if (v)
            {
                Update(null, null);
                _updateTimer.Start();
            }
            else
            {
                _updateTimer?.Stop();
                UpdateItems();
                _oldSchedule = null;
            }
        }

        private async void Update(object sender, EventArgs eventArgs)
        {
            var currentSchedule = await Repo.Schedules.GetCurrentSchedule();
            if (currentSchedule != null)
            {
                if (currentSchedule.Id != _oldSchedule?.Id)
                {
                    _oldSchedule = currentSchedule;
                    UpdateOne(currentSchedule);
                    _needUpdate = false;
                }
                if (_needUpdate)
                {
                    _needUpdate = false;
                    UpdateOne(currentSchedule);
                }
            }
        }

        private void UpdateOne(Schedule currentSchedule)
        {
            Items = new ObservableCollection<WorkGroup>();

            var wgs = currentSchedule?.WorkGroups?.Convert();
            if (wgs?.Count > 0)
                foreach (var workGroup in wgs)
                {
                    Items.Add(workGroup);
                }
        }

        protected override void Add()
        {
            new WorkGroupAddView().Show();
        }

        protected override void Change()
        {
            if (SelectedItem != null)
                new WorkGroupChangeView(SelectedItem).Show();
        }

        protected override async void Remove()
        {
            if (SelectedItem != null)
                await Repo.WorkGroups.Delete(SelectedItem);
        }

        
    }
}