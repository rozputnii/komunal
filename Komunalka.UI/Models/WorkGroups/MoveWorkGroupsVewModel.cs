using System;
using System.Collections.ObjectModel;
using Komunalka.DAL;
using Komunalka.Models.Base;

namespace Komunalka.Models.WorkGroups
{
    public class MoveWorkGroupsVewModel : MoveItemBaseViewModel<WorkGroup>
    {
        public MoveWorkGroupsVewModel(ObservableCollection<WorkGroup> sourceCollection, ObservableCollection<WorkGroup> destinationCollection, Action<WorkGroup> toDestinationCallback, Action<WorkGroup> toSourceCallback) : base(sourceCollection, destinationCollection, toDestinationCallback, toSourceCallback)
        {
        }
    }
}