﻿using Komunalka.DAL;

namespace Komunalka.Models.WorkGroups
{
    /// <summary>
    /// Interaction logic for AddPerson.xaml
    /// </summary>
    public partial class WorkGroupChangeView
    {
        public WorkGroupChangeView(WorkGroup workGroup)
        {
            InitializeComponent();
            DataContext = new WorkGroupChangeViewModel(workGroup);
        }

    }
}
