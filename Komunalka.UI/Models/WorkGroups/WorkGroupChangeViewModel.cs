﻿using System.Linq;
using System.Windows;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;
using Komunalka.Models.Employees;

namespace Komunalka.Models.WorkGroups
{
    public class WorkGroupChangeViewModel : OneElementBase<WorkGroup>
    {
        public WorkGroupChangeViewModel(WorkGroup workGroup) : base(Repo.WorkGroups,() => workGroup)
        {
         LoadEmployees();
        }
      
        private Employee _selectedFreeEmployee;
        private Employee _selectedInGroupEmployee;

        protected override async void ActionMethod(object obj)
        {
            Item.Employees = MoveModel?.DestinationCollection;
            await Repository.Update(Item);
            (obj as Window)?.Close();
        }

    

        public void LoadEmployees()
        {
            try
            {
                var items = Repo.Employees.GetAll;
                if (items != null)
                {
                    var freeItems = items.Where(o => o.WorkGroup == null);
                    MoveModel = new MoveEmployeesVewModel(freeItems?.Convert(), Item?.Employees?.Convert());
                }
            }
            catch
            {

            }
        }

         public MoveEmployeesVewModel MoveModel { get; set; }
      
    }
}
