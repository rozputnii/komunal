﻿using System.Linq;
using System.Windows;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;
using Komunalka.Models.Employees;

namespace Komunalka.Models.WorkGroups
{
    public class WorkGroupAddViewModel: OneElementBase<WorkGroup>
    {
        public WorkGroupAddViewModel() : base(Repo.WorkGroups,() => new WorkGroup {Name = "Нова бригада"})
        {
           LoadEmployees();
        }
        private Employee _selectedFreeEmployee;
        private Employee _selectedInGroupEmployee;

        protected override async void ActionMethod(object obj)
        {
            Item.Employees = MoveModel.DestinationCollection;
            await Repository.Add(Item);
            (obj as Window)?.Close();
        }

       
       

        public  void LoadEmployees()
        {
            try
            {
               var items =  Repo.Employees.GetAll;
                if (items != null)
                {
                    var freeItems = items.Where(o => o.WorkGroup == null);
                    MoveModel=new MoveEmployeesVewModel(freeItems?.Convert(),null);
                }
            }
            catch
            {

            }
        }
        public MoveEmployeesVewModel MoveModel { get; set; }
    }
}
