﻿using System;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.WorkGroups
{
    public class WorkGroupShowViewModel : OneElementBase<WorkGroup>
    {
        protected WorkGroupShowViewModel(Func<WorkGroup> loadFunc) : base(Repo.WorkGroups,loadFunc)
        {
        }
      
    }
}
