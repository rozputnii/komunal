﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using GemBox.Document;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;
using Komunalka.Models.KindOfRequests;
using Komunalka.Models.TypeOfWorks;
using Komunalka.Services;
using Microsoft.Win32;

namespace Komunalka.Models.Requests
{
    public class RequestsViewModel: ManyElementBase<Request>
    {
        private bool _allKindOfRequests;
        private KindOfRequest _selectedKindOfRequest;
        private RequestInArchive _selectedRequestInArchive;
       

        protected override void Add()
        {
            new RequestAddView().Show();
        }

        public RequestsViewModel() : base(Repo.Requests)
        {
            Repo.RequestInArchives.Changed += (sender, args) => LoadRequestInArchives();
            Repo.Requests.Changed += RequestsOnChanged;
            Repo.KindOfRequests.Changed += RequestsOnChanged;
            _allKindOfRequests = true;
            LoadKindOfRequests();
            LoadRequests();
            LoadRequestInArchives();
            FilterCommand=new Command(Find);
            ClearCommand=new Command(ClearFilter);
        }

        private  void ClearFilter(object o)
        {
            RequestInArchives = ( Repo.RequestInArchives.GetAll)?.Convert();
        }

        private  void Find(object o)
        {
            var el = o as string;
            el = el?.ToLower();
            if(el==null)return;
            
            RequestInArchives?.Clear();
            if (FindOnId)
            {
                RequestInArchives =
               ( Repo.RequestInArchives.GetAll)?.Where(archive => archive.RequestId.ToString().ToLower().StartsWith(el))?
                   .Convert();
            }
            else
            {
                 RequestInArchives =
                ( Repo.RequestInArchives.GetAll)?.Where(archive => archive.Name?.ToLower().StartsWith(el)??false)?
                    .Convert();
            }
           
        }

        private void RequestsOnChanged(object sender, EventArgs eventArgs)
        {
            LoadKindOfRequests();
            LoadRequests();
        }

        public  void LoadKindOfRequests()
        {
            var items =  Repo.KindOfRequests.GetAll;
            KindOfRequests = items?.Convert();
        }

        public  void LoadRequests()
        {
            var items =  Repo.Requests.GetAll;
            Requests = items?.Convert();
        }
        public ObservableCollection<RequestInArchive> RequestInArchives { get; set; }

        public RequestInArchive SelectedRequestInArchive
        {
            get { return _selectedRequestInArchive; }
            set
            {
                _selectedRequestInArchive = value;
                RequestInArchiveIsSelected=value!=null;
                Raise();
            }
        }
       public bool RequestInArchiveIsSelected { get; set; }
        public  void LoadRequestInArchives()
        {
            var items =  Repo.RequestInArchives.GetAll;
            RequestInArchives = items?.Convert();
        }
        public ObservableCollection<Request> Requests { get; set; }

        public bool AllKindOfRequests
        {
            get { return _allKindOfRequests; }
            set
            {
                _allKindOfRequests = value;
                if (value) LoadRequests();
                Raise();
            }
        }

        public KindOfRequest SelectedKindOfRequest
        {
            get { return _selectedKindOfRequest; }
            set
            {
                _selectedKindOfRequest = value;
                SelectRequests(value);
                Raise();
            }
        }

        private void SelectRequests(KindOfRequest kind)
        {
            if (kind != null)
            {
                Requests = new ObservableCollection<Request>();
                if (kind.Requests.Any())
                {
                    foreach (var request in kind.Requests)
                    {
                        if (!request.InArchive)
                            Requests.Add(request);
                    }
                }
            }
        }

        public ObservableCollection<KindOfRequest> KindOfRequests { get; set; }

        public ICommand ManageTypeOfWorksCommand { get; set; } = new Command(() => {new TypeOfWorksView().Show(); });

        public ICommand AddKindOfRequestCommand { get; set; }=new Command(()=> {new KindOfRequestAddView().Show();});

        public ICommand RemoveKindOfRequestCommand { get; set; } = new Command(async o =>
        {
            var el = o as KindOfRequest;
            if (el != null)
            {
                await Repo.KindOfRequests.Delete(el);
            }
        });
        public ICommand PrintArhiveCommand { get; set; } =new Command(o =>
        {
            var dk = o as RequestInArchive;
            if (dk != null)
            {
                var dlg = new SaveFileDialog();
                dlg.Filter = "Word files (*.docx)|*.docx|All files (*.*)|*.*";
                dlg.RestoreDirectory = true;
                dlg.ShowDialog();
                var path = dlg.FileName;

                // If using Professional version, put your serial key below.
                ComponentInfo.SetLicense("FREE-LIMITED-KEY");

                var document = new DocumentModel();

                document.Sections.Add(
                    new Section(document,
                        new Paragraph(document,
                            new SpecialCharacter(document, SpecialCharacterType.Tab),
                            new SpecialCharacter(document, SpecialCharacterType.Tab),
                            new Run(document, $"Заявка №{dk.RequestId}"),
                            new SpecialCharacter(document, SpecialCharacterType.LineBreak),
                            new Run(document, $"Назва: {dk.Name}"),
                            new SpecialCharacter(document, SpecialCharacterType.LineBreak),
                            new Run(document, $"ПІБ: {dk.PIB}"),
                            new SpecialCharacter(document, SpecialCharacterType.LineBreak),
                            new Run(document, $"Адреса: {dk.Adress}"),
                            new SpecialCharacter(document, SpecialCharacterType.LineBreak),
                            new Run(document, $"Телефон: {dk.Phone}"),
                            new SpecialCharacter(document, SpecialCharacterType.LineBreak),
                            new Run(document, $"Вид заявки: {dk.KindOfRequestName}"),
                            new SpecialCharacter(document, SpecialCharacterType.LineBreak),
                            new Run(document, $"Тип робіт: {dk.TypeOfWorkName}"),
                            new SpecialCharacter(document, SpecialCharacterType.LineBreak),
                            new Run(document, $"Ремонтна бригада: {dk.WorkGroups}"),
                            new SpecialCharacter(document, SpecialCharacterType.LineBreak),
                            new Run(document, $"Майстри: {dk.Employees}"),
                            new SpecialCharacter(document, SpecialCharacterType.LineBreak),
                            new Run(document, $"Дата: {dk.Date}"),
                            new SpecialCharacter(document, SpecialCharacterType.LineBreak),
                             new Run(document, $"Дата виконання: {dk.DateEnd}"),
                            new SpecialCharacter(document, SpecialCharacterType.LineBreak)
                            )));

                document.Save(path);
            }
        });

        public string FilterReqtext { get; set; }

        public ICommand FilterCommand { get; set; }

        public ICommand ClearCommand { get; set; }

        public bool FindOnId { get; set; }

        public bool FindOnName
        {
            get { return !FindOnId; }
            set { FindOnId = !value; }
        }
    }
}
