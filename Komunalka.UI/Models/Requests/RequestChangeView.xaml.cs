﻿using Komunalka.DAL;

namespace Komunalka.Models.Requests
{
    /// <summary>
    /// Interaction logic for AddPerson.xaml
    /// </summary>
    public partial class RequestChangeView
    {
        public RequestChangeView(Request request)
        {
            InitializeComponent();
            DataContext =new RequestChangeViewModel(request);
        }

    }
}
