﻿using System;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.Requests
{
    public class RequestShowViewModel : OneElementBase<Request>
    {
        protected RequestShowViewModel(Func<Request> loadFunc) : base(Repo.Requests, loadFunc)
        {
        }

      
    }
}
