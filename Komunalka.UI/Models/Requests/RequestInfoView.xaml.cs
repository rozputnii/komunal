﻿using Komunalka.DAL;

namespace Komunalka.Models.Requests
{
    /// <summary>
    /// Interaction logic for AddPerson.xaml
    /// </summary>
    public partial class RequestInfoView
    {
        public RequestInfoView(Request request)
        {
            InitializeComponent();
            DataContext =new RequestInfoViewModel(request);
        }

    }
}
