using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.Requests
{
    public class RequestInfoViewModel : OneElementBase<Request>
    {
        public RequestInfoViewModel(Request request) : base(Repo.Requests, ()=>request)
        {
        }
    }
}