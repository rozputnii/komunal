﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;
using Komunalka.Models.Employees;
using Komunalka.Services;

namespace Komunalka.Models.Requests
{
    public class RequestAddViewModel : OneElementBase<Request>
    {
        
        public RequestAddViewModel() : base(Repo.Requests,() => new Request {Date = DateTime.Now})
        {
            ExtraCommand=new Command(o => { ShowExtra = !ShowExtra; });
            LoadKindOfRequests();
            LoadTypeOfWorks();
            LoadWorkGroups();
        }
        protected override async void ActionMethod(object obj)
        {
            Item.Employees = MoveVm.DestinationCollection;
            if (!ShowExtra)
                Item.Date = DateTime.Now;
            else
            {
                Item.Date=new DateTime(Item.Date.Year, Item.Date.Month, Item.Date.Day,DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            }
            await Repository.Add(Item);
            (obj as Window)?.Close();
        }
        public  void LoadKindOfRequests()
        {
            var items =  Repo.KindOfRequests.GetAll;
            KindOfRequests = items?.Convert();
        }
        public  void LoadTypeOfWorks()
        {
            var items =  Repo.TypeOfWorks.GetAll;
            TypeOfWorks = items?.Convert();
        }
        public async void LoadWorkGroups()
        {

            var s = await Repo.Schedules.GetCurrentSchedule();
            WorkGroups = s?.WorkGroups?.Convert();
            MoveVm = new MoveEmployeesVewModel(s?.Employees?.Convert(), null);
        }

        
        public ObservableCollection<KindOfRequest> KindOfRequests { get; set; }
     
        public ObservableCollection<TypeOfWork> TypeOfWorks { get; set; }
        
        public ObservableCollection<WorkGroup> WorkGroups { get; set; }

        public MoveEmployeesVewModel MoveVm { get; set; }

        public bool ShowExtra { get; set; }
        public ICommand ExtraCommand { get; set; }
    }
}
