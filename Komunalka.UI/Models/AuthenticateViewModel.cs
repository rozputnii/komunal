﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models
{
    public class AllViewModel : ViewModelBase
    {
        public AllViewModel()
        {
            OpenSettingsCommand = new Command(() =>
            {
                SettingsIsOpen = !SettingsIsOpen;
            });
        }

        public ICommand OpenSettingsCommand { get; set; } 
        public bool SettingsIsOpen { get; set; }
    }

    public class AuthenticateViewModel:OneElementBase<Manager>
    {
        public static bool NeedCreateNewDb { get; set; }
        
        protected override async void ActionMethod(object obj)
        {
            
            Properties.Settings.Default.UseDefaultAuth = UseDefaultAuth;
            if (UseDefaultAuth)
            {
                Properties.Settings.Default.DefaultLogin = Item.Login;
                Properties.Settings.Default.DefaultPassword = Item.Password;
            }
            else
            {
                Properties.Settings.Default.DefaultLogin = default(string);
                Properties.Settings.Default.DefaultPassword = default(string);
            }
            Properties.Settings.Default.Save();
            Error = false;
            try
            {
                if (NeedCreateNewDb)
                {
                    
                }

                var items =  Repository.GetAll;
                var item = items?.FirstOrDefault(o => o?.Login == Item?.Login);
                if (item?.Login == Item?.Login && item?.Password == Item?.Password)
                {
                    new AllView().Show();
                    (obj as Window)?.Close();
                }
                else
                {
                    Error = true;
                }
            }
            catch (Exception exception)
            {
                Error = true;
            }

        }

        public bool Error { get; set; }

        public bool IsSettingsOpen { get; set; }

        public bool UseDefaultAuth { get; set; } = Properties.Settings.Default.UseDefaultAuth;

        public ICommand SettingsCommand { get; set; }

        public AuthenticateViewModel() : base(Repo.Managers)
        {
            if (Properties.Settings.Default.IsFirstLoading)
                IsSettingsOpen = true;
            SettingsCommand = new Command(() => { IsSettingsOpen = !IsSettingsOpen; });
            if (Properties.Settings.Default.UseDefaultAuth)
            {
                Item.Login = Properties.Settings.Default.DefaultLogin;
                Item.Password = Properties.Settings.Default.DefaultPassword;
            }
            
        }
    }
}
