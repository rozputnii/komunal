﻿using System;
using System.Windows.Input;
using Komunalka.DAL.Repositories;

namespace Komunalka.Models.Base
{
    public abstract class OneElementBase<T> : ViewModelBase where T : class, new()
    {
        protected Repository<T> Repository { get; }

        protected OneElementBase(Repository<T> repository, Func<T> loadFunc)
        {
            ActionCommand = new Command(ActionMethod);
            Item = loadFunc();
            Repository = repository;
        }

        protected OneElementBase(Repository<T> repository) : this(repository, () => new T())
        {
        }

        protected virtual void ActionMethod(object obj)
        {
        }

        public T Item { get; set; }

        public ICommand ActionCommand { get; set; }
    }
}
