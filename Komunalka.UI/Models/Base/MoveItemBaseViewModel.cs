using System;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Komunalka.Models.Base
{
    public abstract class MoveItemBaseViewModel<T>:ViewModelBase where T:class,new()
    {
        protected Action<T> ToDestinationCallback;
        protected Action<T> ToSourceCallback;
        private T _selectedSource;
        private T _selectedDestination;

        protected MoveItemBaseViewModel(ObservableCollection<T> sourceCollection,
            ObservableCollection<T> destinationCollection, Action<T> toDestinationCallback, Action<T> toSourceCallback)
        {
            SourceCollection = sourceCollection??new ObservableCollection<T>();
            DestinationCollection = destinationCollection ?? new ObservableCollection<T>();
            for (int index = 0; index < sourceCollection?.Count; index++)
            {
                var item = sourceCollection[index];
                if (destinationCollection?.Contains(item) ?? false)
                {
                    sourceCollection.Remove(item);
                }
            }
            ToDestinationCallback = toDestinationCallback;
            ToSourceCallback = toSourceCallback;
            ToDestinationCommand = new Command(ToDestination);
            ToSourceCommand = new Command(ToSource);
        }

        public ObservableCollection<T> SourceCollection { get; set; }
        public ObservableCollection<T> DestinationCollection { get; set; }

        public T SelectedSource
        {
            get { return _selectedSource; }
            set
            {
                _selectedSource = value;
                SourceIsSelected = value != null;
                if (SourceIsSelected)
                    DestinationIsSelected = false;
                Raise();

            }
        }

        public bool SourceIsSelected { get; set; }

        public T SelectedDestination
        {
            get { return _selectedDestination; }
            set
            {
                _selectedDestination = value;
                DestinationIsSelected = value != null;
                if (DestinationIsSelected)
                    SourceIsSelected = false;
                Raise();
            }
        }

        public bool DestinationIsSelected { get; set; }
        public  ICommand ToDestinationCommand { get; set; }
        public  ICommand ToSourceCommand { get; set; }

        protected virtual void ToDestination(object o)
        {
            try
            {
                var el = o as T;
                if (el == null) return;
                DestinationCollection.Add(el);
                SourceCollection.Remove(el);
                ToDestinationCallback?.Invoke(el);
            }
            catch
            {

            }
        }

        protected virtual void ToSource(object o)
        {
            try
            {
                var el = o as T;
                if (el == null) return;
                SourceCollection.Add(el);
                DestinationCollection.Remove(el);
                ToSourceCallback?.Invoke(el);
            }
            catch
            {

            }
        }
    }
}