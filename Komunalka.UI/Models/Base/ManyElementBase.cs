﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using Komunalka.DAL.Repositories;

namespace Komunalka.Models.Base
{
    public abstract class ManyElementBase<T> : ViewModelBase where T :class, new()
       
    {
        private T _selectedItem;
        private Repository<T> Repository { get; }

       
        protected ManyElementBase(Repository<T> repository)
        {
            Repository = repository;
            AddCommand = new Command(Add);
            RemoveCommand = new Command(Remove);
            ChangeCommand = new Command(Change);
            ShowCommand = new Command(Show);
            Items = new ObservableCollection<T>();
            UpdateItems();

            //Repository.Changed += (sender, args) => UpdateItems();
        }

        protected virtual void UpdateItems()
        {
            try
            {
                Items=Repository.GetAll?.Convert();
            }
            catch { }
        }


        protected virtual void Add() {}
        protected virtual void Remove() { }
        protected virtual void Change() { }
        protected virtual void Show() { }

        
        public bool ItemSelected { get; set; }

        public ObservableCollection<T> Items { get; set; }

        public virtual T SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                ItemSelected = value != null;
                Raise();
            }
        }

        public ICommand AddCommand { get; set; }
        public ICommand RemoveCommand { get; set; }
        public ICommand ChangeCommand { get; set; }
        public ICommand ShowCommand { get; set; }
    }
}