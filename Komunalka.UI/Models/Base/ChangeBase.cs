﻿using System;
using System.Windows;
using Komunalka.DAL.Repositories;

namespace Komunalka.Models.Base
{
    public abstract class ChangeBase<T> : OneElementBase<T> where T :class, new()
    {
        protected override async void ActionMethod(object obj)
        {
            await Repository.Update(Item);
                (obj as Window)?.Close();
        }

        protected ChangeBase(Repository<T> repository, Func<T> loadFunc) : base(repository, loadFunc)
        {
        }


    }
}
