﻿using System;
using System.Windows;
using Komunalka.DAL.Repositories;

namespace Komunalka.Models.Base
{
    public abstract class AddBase<T> : OneElementBase<T> where T : class, new()
    {
        protected override async void ActionMethod(object obj)
        {
                await Repository.Add(Item);
                (obj as Window)?.Close();
        }


        protected AddBase(Repository<T> repository, Func<T> loadFunc) : base(repository,loadFunc)
        {
        }
        protected AddBase(Repository<T> repository) : base(repository)
        {
        }
    }
}
