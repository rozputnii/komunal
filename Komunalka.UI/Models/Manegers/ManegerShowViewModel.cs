﻿using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.Manegers
{
    public class ManegerShowViewModel : OneElementBase<Manager>
    {
        public ManegerShowViewModel() : base(Repo.Managers)
        {
        }
    }
}