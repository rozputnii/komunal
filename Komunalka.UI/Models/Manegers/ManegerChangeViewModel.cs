﻿using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.Manegers
{
    public class ManegerChangeViewModel : OneElementBase<Manager>
    {
        public ManegerChangeViewModel() : base(Repo.Managers)
        {
        }
    }
}