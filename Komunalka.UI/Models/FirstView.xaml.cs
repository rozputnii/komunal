﻿using System.Globalization;
using System.Threading;
using Komunalka.DAL.Repositories;

namespace Komunalka.Models
{
    /// <summary>
    /// Interaction logic for FirstView.xaml
    /// </summary>
    public partial class FirstView 
    {
        public FirstView()
        {
            InitializeComponent();
            Loading();
        }

        public async void Loading()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("uk-UA");
            Thread.CurrentThread.CurrentCulture = new CultureInfo("uk-UA");
            var settings = new DbSettings
            {
                CreateDbIfNotExists = Properties.Settings.Default.CreateDbIfNotExists,
                ServerName = Properties.Settings.Default.ServerName,
                DbName = Properties.Settings.Default.DbName,
                UseLocalServer = Properties.Settings.Default.UseLocalServer
            };

            await Repo.Init(settings);
            new AuthenticateView().Show();
            Close();

        }
       
    }
}
