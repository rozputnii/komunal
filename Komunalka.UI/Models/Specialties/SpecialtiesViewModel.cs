﻿using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.Specialties
{
    public class SpecialtiesViewModel : ManyElementBase<Specialty>
    {
        public SpecialtiesViewModel() : base(Repo.Specialties)
        {
        }
    }
}
