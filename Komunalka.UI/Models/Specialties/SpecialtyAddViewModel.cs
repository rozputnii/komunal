﻿using Komunalka.DAL;
using Komunalka.DAL.Repositories;
using Komunalka.Models.Base;

namespace Komunalka.Models.Specialties
{
    public class SpecialtyAddViewModel : AddBase<Specialty>
    {
        public SpecialtyAddViewModel() : base(Repo.Specialties)
        {
        }
    }
}