//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Komunalka.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Request
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Request()
        {
            this.InArchive = false;
            this.Employees = new HashSet<Employee>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
        public string Description { get; set; }
        public int KindOfRequestId { get; set; }
        public int TypeOfWorkId { get; set; }
        public System.DateTime Date { get; set; }
        public Nullable<int> ManagerId { get; set; }
        public string Phone { get; set; }
        public string PIB { get; set; }
        public int Status { get; set; }
        public Nullable<System.DateTime> DateEnd { get; set; }
        public Nullable<int> WorkGroupId { get; set; }
        public bool InArchive { get; set; }
    
        public virtual KindOfRequest KindOfRequest { get; set; }
        public virtual Manager Manager { get; set; }
        public virtual TypeOfWork TypeOfWork { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Employee> Employees { get; set; }
        public virtual WorkGroup WorkGroup { get; set; }
    }
}
