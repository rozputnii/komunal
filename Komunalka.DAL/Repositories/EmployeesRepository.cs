using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;

namespace Komunalka.DAL.Repositories
{
    public class EmployeesRepository:Repository<Employee>
    {
         public override  Task Add(Employee entity)
        {
            return base.Update(o =>
            {
                entity.Id = default(int);
                o.Employees.Add(entity);
            });
        }

        public override  Task Delete(Employee entity)
        {
            return base.Update(o =>
            {
                o.Employees.Remove(entity);
            });
        }

        public override Task Update(Employee entity)
        {
            return base.Update(o =>
            {
                o.Employees.AddOrUpdate(entity);
            });
        }

        public override IEnumerable<Employee> GetAll=>Context?.Employees as IEnumerable<Employee>;
        

        public override Employee FindById(int id)
        {
            return Context?.Employees?.FirstOrDefault(o => o.Id == id);
        }

        public EmployeesRepository(IDbProvider provider) : base(provider)
        {
        }
    }
}