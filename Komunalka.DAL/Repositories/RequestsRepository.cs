using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;

namespace Komunalka.DAL.Repositories
{
    public class RequestsRepository : Repository<Request>
    {
        public override IEnumerable<Request> GetAll =>

            Context?.Requests?.Where(request => !request.InArchive) as IEnumerable<Request>;
        
        public IEnumerable<Request> GetAllInArhive=>Context?.Requests?.Where(request => request.InArchive) as IEnumerable<Request>;
        

        public IEnumerable<Request> All => Context?.Requests as IEnumerable<Request>;
    

        public override Request FindById(int id)
        {
            return Context?.Requests?.FirstOrDefault(o => o.Id == id);
        }

        public override Task Add(Request entity)
        {
            return base.Update(o =>
            {
                entity.Id = default(int);
                o.Requests.Add(entity);
                AddChange(entity,o);
            });
        }

        public override  Task Delete(Request entity)
        {
            return base.Update(o =>
            {
                o.Requests.Remove(entity);
                DeleteChange(entity, o);
            });
        }

        public static void DeleteChange(Request entity, KModelContainer1 o)
        {
            if (entity == null) return;
            if (entity.Status != 2)
            {
                if (entity.Employees?.Count > 0)
                    foreach (var employee in entity.Employees)
                    {

                        if (employee.CountAllReq > 0)
                        {
                            employee.CountAllReq--;
                            o.Employees.AddOrUpdate(employee);
                        }
                    }
                if (entity.WorkGroup?.Employees?.Count > 0)
                    foreach (var employee in entity.WorkGroup.Employees)
                    {
                        if (employee.CountAllReq > 0)
                        {
                            employee.CountAllReq--;
                            o.Employees.AddOrUpdate(employee);
                        }
                    }
            }
        }

        public static void AddChange(Request request, KModelContainer1 o)
        {
            if (request?.Employees?.Count > 0)
                foreach (var employee in request.Employees)
                {
                    if (employee.CountAllReq == null)
                        employee.CountAllReq = 0;
                    employee.CountAllReq++;
                    o.Employees.AddOrUpdate(employee);
                }
            if (request?.WorkGroup?.Employees?.Count > 0)
                foreach (var employee in request.WorkGroup.Employees)
                {
                    if (employee.CountAllReq == null)
                        employee.CountAllReq = 0;
                    employee.CountAllReq++;
                    o.Employees.AddOrUpdate(employee);
                }
        }
        public static void UpdateChange(Request new_, KModelContainer1 o)
        {
            var old = o.Requests?.FirstOrDefault(request => request.Id == new_.Id);

            if (old != null)
            {
                ChangeEmployees(old, new_, o);
                ChangeWorkGroups(old, new_, o);
            }
        }
        private static void ChangeEmployees(Request oldRequest, Request newRequest, KModelContainer1 o)
        {
            var oldEmployees = oldRequest.Employees;
            var newEmployees = newRequest.Employees;

            if (newEmployees?.Count > 0)
            {
                foreach (var newE in newEmployees)
                {
                    var hasChanges = false;
                    if (newRequest.Status == 2)
                    {
                        if (newE.CountDoneReq == null)
                            newE.CountDoneReq = 0;
                        newE.CountDoneReq++;
                        hasChanges = true;
                    }

                    var contains = oldEmployees?.Contains(newE) ?? false;
                    if (!contains)
                    {
                        if (newE.CountAllReq == null)
                            newE.CountAllReq = 0;
                        newE.CountAllReq++;
                        hasChanges = true;
                    }

                    if (hasChanges)
                        o.Employees.AddOrUpdate(newE);
                }
            }

            if (oldEmployees?.Count > 0)
                foreach (var oldE in oldEmployees)
                {
                    var contains = newEmployees?.Contains(oldE) ?? false;
                    if (!contains)
                    {
                        if (oldE.CountAllReq == null)
                            oldE.CountAllReq = 0;
                        oldE.CountAllReq--;
                        o.Employees.AddOrUpdate(oldE);
                    }
                }
        }
        private static void ChangeWorkGroups(Request oldRequest, Request newRequest, KModelContainer1 o)
        {
            var oldEmployees = oldRequest.WorkGroup?.Employees;
            var newEmployees = newRequest.WorkGroup?.Employees;

            if (newEmployees?.Count > 0)
            {
                foreach (var newE in newEmployees)
                {
                    var hasChanges = false;
                    var contains = oldEmployees?.Contains(newE) ?? false;
                    if (!contains)
                    {
                        if (newE.CountAllReq == null)
                            newE.CountAllReq = 0;
                        newE.CountAllReq++;
                        hasChanges = true;
                    }
                    if (newRequest.Status == 2)
                    {
                        if (newE.CountDoneReq == null)
                            newE.CountDoneReq = 0;
                        newE.CountDoneReq++;
                        hasChanges = true;
                    }
                    if(hasChanges)
                    o.Employees.AddOrUpdate(newE);
                }
            }

            if (oldEmployees?.Count > 0)
                foreach (var oldE in oldEmployees)
                {
                    var contains = newEmployees?.Contains(oldE) ?? false;
                    if (!contains)
                    {
                        if (oldE.CountAllReq == null)
                            oldE.CountAllReq = 0;
                        oldE.CountAllReq--;
                        o.Employees.AddOrUpdate(oldE);
                    }
                }
        }
        
            

        public override  Task Update(Request entity)
        {
            return base.Update(o =>
            {
                o.Requests.AddOrUpdate(entity);
                UpdateChange(entity, o);
            });
        }


        public RequestsRepository(IDbProvider provider) : base(provider)
        {
        }
    }
}