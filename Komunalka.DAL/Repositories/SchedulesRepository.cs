﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;

namespace Komunalka.DAL.Repositories
{
    public class SchedulesRepository :Repository<Schedule>
    {
        public override   Task Add(Schedule entity)
        {
            return base.Update(o =>
            {
                entity.Id = default(int);
                o.Schedules.Add(entity);
            });
        }

        public override  Task Delete(Schedule entity)
        {
            return base.Update(o =>
            {
                o.Schedules.Remove(entity);
            });
        }

        public override  Task Update(Schedule entity)
        {
            return base.Update(o =>
            {
                o.Schedules.AddOrUpdate(entity);
            });
        }

        public override IEnumerable<Schedule> GetAll => Context.Schedules as IEnumerable<Schedule>;
        

        public override Schedule FindById(int id)
        {
            return Context?.Schedules?.FirstOrDefault(o => o.Id == id);
        }

        public Task<Schedule> GetCurrentSchedule()
        {
            return Task.Run( () =>
            {
                try
                {
                    var s = GetAll?.ToArray();
                    return
                        s?.FirstOrDefault(
                            schedule => schedule.TimeStart() < DateTime.Now && schedule.TimeEnd() > DateTime.Now);
                }
                catch
                {
                    return null;
                }
            });
        }
        public SchedulesRepository(IDbProvider provider) : base(provider)
        {
        }
    }
}

