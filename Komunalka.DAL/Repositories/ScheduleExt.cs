using System;

namespace Komunalka.DAL.Repositories
{
    public static class ScheduleExt
    {
        public static DateTime TimeStart(this Schedule schedule)
        {
            int hour;
            switch (schedule.Kind)
            {
                case 1:
                    hour = 13;
                    break;
                case 2:
                    hour = 20;
                    break;
                default:
                    hour = 6;
                    break;
            }
            return new DateTime(schedule.Date.Year, schedule.Date.Month, schedule.Date.Day, hour, 0, 0);
        }

        public static DateTime TimeEnd(this Schedule schedule)
        {
            int hour;
            switch (schedule.Kind)
            {
                case 1:
                    hour = 20;
                    break;
                case 2:
                {
                    hour = 6;
                    var v = schedule.Date.AddDays(1);
                    return new DateTime(v.Year, v.Month, v.Day, hour, 0, 0);
                }
                default:
                    hour = 13;
                    break;
            }
           return new DateTime(schedule.Date.Year, schedule.Date.Month, schedule.Date.Day, hour, 0, 0);
        }
    }
}
