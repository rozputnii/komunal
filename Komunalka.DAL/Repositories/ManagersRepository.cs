using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;

namespace Komunalka.DAL.Repositories
{
    public class ManagersRepository : Repository<Manager>
    {
        public override  Task Add(Manager entity)
        {
            return base.Update(o =>
            {
                entity.Id = default(int);
                o.Managers.Add(entity);
            });
        }

        public override  Task Delete(Manager entity)
        {
            return base.Update(o =>
            {
                o.Managers.Remove(entity);
            });
        }

        public override  Task Update(Manager entity)
        {
            return base.Update(o =>
            {
                o.Managers.AddOrUpdate(entity);
            });
        }

        public override IEnumerable<Manager> GetAll => Context?.Managers as IEnumerable<Manager>;

        public override Manager FindById(int id)
        {
            return Context?.Managers?.FirstOrDefault(o => o.Id == id);
        }

        public ManagersRepository(IDbProvider provider) : base(provider)
        {
        }
    }
}