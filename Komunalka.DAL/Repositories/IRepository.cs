﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Komunalka.DAL.Repositories
{
    public interface IRepository<T> 
    {
        Task<IEnumerable<T>> GetAll();
        void Add(T entity);
        void Delete(T entity);
        void Update(T entity);
        Task<T> FindById(int id);

    }
}
