using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;

namespace Komunalka.DAL.Repositories
{
    public class TypeOfWorksRepository : Repository<TypeOfWork>
    {
       
        public override  Task Add(TypeOfWork entity)
        {
            return base.Update(o =>
            {
                entity.Id = default(int);
                o.TypeOfWorks.Add(entity);
            });
        }

        public override  Task Delete(TypeOfWork entity)
        {
            return base.Update(o =>
            {
                o.TypeOfWorks.Remove(entity);
            });
        }

        public override  Task Update(TypeOfWork entity)
        {
            return base.Update(o =>
            {
                o.TypeOfWorks.AddOrUpdate(entity);
            });
        }

        public override IEnumerable<TypeOfWork> GetAll=> Context?.TypeOfWorks as IEnumerable<TypeOfWork>;
        

        public override TypeOfWork FindById(int id)
        {
            return Context?.TypeOfWorks?.FirstOrDefault(o => o.Id == id);
        }

        public TypeOfWorksRepository(IDbProvider provider) : base(provider)
        {
        }
    }
}