using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Komunalka.DAL.Repositories
{
    public static class ConvertExt
    {
        public static ObservableCollection<T> Convert<T>(this IEnumerable<T> arr) 
        {
            var o = new ObservableCollection<T>();
            try
            {
                var enumerable = arr as T[] ?? arr.ToArray();
                for (var i = 0; i < enumerable?.Length; i++)
                {
                    o.Add(enumerable[i]);
                }
            }
            catch
            {

            }
            return o;
        }
    }
}