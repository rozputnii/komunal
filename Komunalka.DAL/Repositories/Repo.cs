﻿using System.Threading.Tasks;

namespace Komunalka.DAL.Repositories
{
    public class Repo
    {
        //static Repo()
        //{
        //    InitDb();
        //    Init();
        //}
        public static Task Init(DbSettings settings)
        {
            return Task.Run(() =>
            {
                Provider = new DbProvider(settings);
                WorkGroups = new WorkGroupsRepository(Provider);
                Employees = new EmployeesRepository(Provider);
                KindOfRequests = new KindOfRequestsRepository(Provider);
                Managers = new ManagersRepository(Provider);
                MaterialBases = new MaterialBasesRepository(Provider);
                Requests = new RequestsRepository(Provider);
                Schedules = new SchedulesRepository(Provider);
                Specialties = new SpecialtiesRepository(Provider);
                TypeOfWorks = new TypeOfWorksRepository(Provider);
                Materials = new MaterialsRepository(Provider);
                Instruments = new InstrumentsRepository(Provider);
                RequestInArchives = new RequestInArchivesRepository(Provider);
            });

        }

        public static void OnChangedAll()
        {
             Task.Run(() =>
            {
                WorkGroups?.OnChanged();
                Employees?.OnChanged();
                KindOfRequests?.OnChanged();
                Managers?.OnChanged();
                MaterialBases?.OnChanged();
                Requests?.OnChanged();
                Schedules?.OnChanged();
                Specialties?.OnChanged();
                TypeOfWorks?.OnChanged();
                Materials?.OnChanged();
                Instruments?.OnChanged();
                RequestInArchives?.OnChanged();
            });
        }
        public static DbProvider Provider { get; private set; }
        public static WorkGroupsRepository WorkGroups { get; private set; } 
        public static EmployeesRepository Employees { get; private set; } 
        public static KindOfRequestsRepository KindOfRequests { get; private set; } 
        public static ManagersRepository Managers { get; private set; } 
        public static MaterialBasesRepository MaterialBases { get; private set; } 
        public static RequestsRepository Requests { get; private set; }
        public static RequestInArchivesRepository RequestInArchives { get; private set; } 
        public static SchedulesRepository Schedules { get; private set; } 
        public static SpecialtiesRepository Specialties { get; private set; } 
        public static TypeOfWorksRepository TypeOfWorks { get; private set; } 
        public static MaterialsRepository Materials { get; private set; }
        public static InstrumentsRepository Instruments { get; private set; } 

    }
}
