using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;

namespace Komunalka.DAL.Repositories
{
    public class WorkGroupsRepository : Repository<WorkGroup>
    {

        public override Task Add(WorkGroup entity)
        {
            return base.Update(o =>
            {
                entity.Id = default(int);
                o.WorkGroups.Add(entity);
            });
        }

        public override  Task Delete(WorkGroup entity)
        {
            return base.Update(o =>
            {
                o.WorkGroups.Remove(entity);
            });
        }

        public override  Task Update(WorkGroup entity)
        {
            return base.Update(o =>
            {
                o.WorkGroups.AddOrUpdate(entity);
            });
        }

        public override IEnumerable<WorkGroup> GetAll => Context?.WorkGroups as IEnumerable<WorkGroup>;

      

        public override WorkGroup FindById(int id)
        {
            return Context?.WorkGroups?.FirstOrDefault(o => o.Id == id);
        }

        public WorkGroupsRepository(IDbProvider provider) : base(provider)
        {
        }
    }
}