using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;

namespace Komunalka.DAL.Repositories
{
    public class RequestInArchivesRepository : Repository<RequestInArchive>
    {
        public override IEnumerable<RequestInArchive> GetAll=>        
            Context?.RequestInArchives as IEnumerable<RequestInArchive>;
        

        public override RequestInArchive FindById(int id) => Context?.RequestInArchives?.FirstOrDefault(o => o.Id == id);
      

        public override  Task Add(RequestInArchive entity)
        {
            return base.Update(o =>
            {
                entity.Id = default(int);
                o.RequestInArchives.Add(entity);
            });
        }

        public override  Task Delete(RequestInArchive entity)
        {
            return base.Update(o =>
            {
                o.RequestInArchives.Remove(entity);
            });
        }

        public override  Task Update(RequestInArchive entity)
        {
            return base.Update(o =>
            {
                o.RequestInArchives.AddOrUpdate(entity);
            });
        }


        public RequestInArchivesRepository(IDbProvider provider) : base(provider)
        {
        }
    }
}