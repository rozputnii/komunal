﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;

namespace Komunalka.DAL.Repositories
{
    
    public class SpecialtiesRepository : Repository<Specialty>
    {

        public override  Task Add(Specialty entity)
        {
            return base.Update(o =>
            {
                entity.Id = default(int);
                o.Specialties.Add(entity);
            });
        }

        public override  Task Delete(Specialty entity)
        {
            return base.Update(o =>
            {
                o.Specialties.Remove(entity);
            });
        }

        public override Task Update(Specialty entity)
        {
            return base.Update(o =>
                {
                    o.Specialties.AddOrUpdate(entity);
                });
        }

        public override IEnumerable<Specialty> GetAll => Context?.Specialties as IEnumerable<Specialty>;
        

        public override Specialty FindById(int id)
        {
            return Context?.Specialties?.FirstOrDefault(o => o.Id == id);
        }

        public SpecialtiesRepository(IDbProvider provider) : base(provider)
        {
        }
    }
}