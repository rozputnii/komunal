using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;

namespace Komunalka.DAL.Repositories
{
    public class MaterialBasesRepository : Repository<MaterialBase>
    {
        public override IEnumerable<MaterialBase> GetAll =>
            Context?.MaterialBases as IEnumerable<MaterialBase>;

        public override  Task Add(MaterialBase entity)
        {
            return base.Update(o =>
            {
                entity.Id = default(int);
                o.MaterialBases.Add(entity);
            });
        }

        public override Task Delete(MaterialBase entity)
        {
            return base.Update(o =>
            {
                o.MaterialBases.Remove(entity);
            });
        }

        public override Task Update(MaterialBase entity)
        {
            return base.Update(o =>
            {
                o.MaterialBases.AddOrUpdate(entity);
            });
        }

        public override MaterialBase FindById(int id)
        {
            return Context?.MaterialBases?.FirstOrDefault(o => o.Id == id);
        }


        public MaterialBasesRepository(IDbProvider provider) : base(provider)
        {
        }
    }
}