﻿using System;
using System.Threading.Tasks;

namespace Komunalka.DAL.Repositories
{
    public interface IDbProvider
    {
        KModelContainer1 Context { get; }
        //KModelContainer1 Context { get; }
        
        Task Update(Action<KModelContainer1> action, Action onChangeAction, bool notifySubscribers = true);
    }
}