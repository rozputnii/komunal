using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;

namespace Komunalka.DAL.Repositories
{
    public class InstrumentsRepository : Repository<Instrument>
    {
        public override  Task Add(Instrument entity)
        {
            return base.Update(o =>
            {
                entity.Id = default(int);
                o.Instruments.Add(entity);
            });
        }

        public override  Task Delete(Instrument entity)
        {
            return base.Update(o =>
            {
                o.Instruments.Remove(entity);
            });
        }

        public override  Task Update(Instrument entity)
        {
            return base.Update(o =>
            {
                o.Instruments.AddOrUpdate(entity);
            });
        }
        public override IEnumerable<Instrument> GetAll=> Context?.Instruments as IEnumerable<Instrument>;

        public override Instrument FindById(int id)
        {
            return Context?.Instruments?.FirstOrDefault(o => o.Id == id);
        }

        public InstrumentsRepository(IDbProvider provider) : base(provider)
        {
        }
    }
}