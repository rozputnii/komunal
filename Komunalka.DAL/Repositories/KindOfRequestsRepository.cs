﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;

namespace Komunalka.DAL.Repositories
{
    public class KindOfRequestsRepository : Repository<KindOfRequest>
    {

        public override  Task Add(KindOfRequest entity)
        {
            return base.Update(o =>
            {
                entity.Id = default(int);
                o.KindOfRequests.Add(entity);
            });
        }

        public override  Task Delete(KindOfRequest entity)
        {
            return base.Update(o =>
            {
                o.KindOfRequests.Remove(entity);
            });
        }

        public override  Task Update(KindOfRequest entity)
        {
            return base.Update(o =>
            {
                o.KindOfRequests.AddOrUpdate(entity);
            });
        }

        public override IEnumerable<KindOfRequest> GetAll => Context?.KindOfRequests as IEnumerable<KindOfRequest>;
           

        public override KindOfRequest FindById(int id)
        {
            return Context?.KindOfRequests?.FirstOrDefault(o => o.Id == id);
        }

        public KindOfRequestsRepository(IDbProvider provider) : base(provider)
        {
        }
    }
}