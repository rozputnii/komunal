using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;

namespace Komunalka.DAL.Repositories
{
    public class MaterialsRepository : Repository<Material>
    {
        public MaterialsRepository(IDbProvider provider) : base(provider)
        {

        }

        public override  Task Add(Material entity)
        {
            return base.Update(o => { o.Materials.Add(entity); });
        }

        public override  Task Delete(Material entity)
        {
            return base.Update(o => { o.Materials.Remove(entity); });
        }

        public override  Task Update(Material entity)
        {
            return base.Update(o => { o.Materials.AddOrUpdate(entity); });
        }

        public override IEnumerable<Material> GetAll => Context.Materials as IEnumerable<Material>;


        public override Material FindById(int id)
        {
            return Context.Materials?.FirstOrDefault(o => o.Id == id);
        }

        public IEnumerable<Material> Get(Func<Material, bool> predicate)
        {
            return Context.Materials.Where(predicate);
        }
       
    }
}