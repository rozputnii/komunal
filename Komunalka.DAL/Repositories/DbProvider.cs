﻿using System;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;

namespace Komunalka.DAL.Repositories
{

    public class DbSettings
    {
        public  bool CreateDbIfNotExists { get; set; } = true;
        public  string ServerName { get; set; }
        public  string DbName { get; set; }
        public  bool UseLocalServer { get; set; } = true;
    }

    public class DbProvider : IDbProvider
    {
        public DbProvider(DbSettings settings)
        {
            JustCreateContext();
            Settings = settings;
        }

        public DbSettings Settings { get; set; }
        public Task<bool> CreateIfNotExists(bool create)
        {
            return Task.Run(() =>
            {
                try
                {
                    if (create)
                    {
                        var r = Context.Database.CreateIfNotExists();
                        return r;
                    }
                }
                catch
                {
                    return false;
                }
                return false;
            });
        }

        //public KModelContainer1 Context => _context;
        private KModelContainer1 _context;
        public KModelContainer1 Context => _context;

        private ushort _countChanges;
        private static readonly object SyncObject = new object();

        public string ConnectionString
        {
            get
            {
                
                if (Settings.UseLocalServer)
                    return Settings.DbName;
                var v = new SqlConnectionStringBuilder
                {
                    DataSource = Settings.ServerName,
                    InitialCatalog = Settings.DbName,
                    IntegratedSecurity = true,
                    MultipleActiveResultSets = true
                };

                return v.ConnectionString;
            }
        }

        private void CheckForUpdate()
        {
            _countChanges++;
            if (_countChanges >= 100)
            {
                CreateContext();
            }
        }

        public void JustCreateContext()
        {
            try
            {
                if (ConnectionString != null)
                {
                    var context = new KModelContainer1(ConnectionString);
                    _context?.Dispose();
                    Interlocked.Exchange(ref _context, context);
                    _countChanges = 0;
                }
            }
            catch
            {

            }
        }

        public void CreateContext()
        {
            try
            {
                if (ConnectionString != null)
                {
                    JustCreateContext();
                    Repo.OnChangedAll();
                }
            }
            catch (Exception exception)
            {

            }
        }

        public Task Update(Action<KModelContainer1> action, Action onChangeAction, bool notifySubscribers = true)
        {
            return Task.Run(() =>
            {
                try
                {
                    lock (SyncObject)
                    {
                        action(_context);
                        _context?.SaveChanges();
                        CheckForUpdate();
                    }
                    if (notifySubscribers) onChangeAction();
                }
                catch(Exception exception)
                {
                    lock (SyncObject)
                    {
                        CreateContext();
                    }
                }
            });
            
        }
    }
}