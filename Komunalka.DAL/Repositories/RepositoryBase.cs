﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Komunalka.DAL.Repositories
{
    //public abstract class RepositoryBase
    //{
    //    static RepositoryBase()
    //    {
    //        CreateContext();
    //    }

    //    public event EventHandler Changed; 

    //    private static KModelContainer1 _context;

    //    private static ushort _countChanges;

    //    protected Task Update(Action<KModelContainer1> action, bool notifySubscribers = true)
    //    {
    //        return new Action(() =>
    //        {
    //            try
    //            {
    //                lock (SyncObject)
    //                {
    //                    action(Context);
    //                    _context.SaveChanges();
    //                }
    //                CheckForUpdate();
    //                if (notifySubscribers) OnChanged();
    //            }
    //            catch
    //            {
    //                CreateContext();
    //            }
    //        }).RunWithDispatcher();
    //    }
        
    //    protected KModelContainer1 Context => _context;

    //    private static readonly object SyncObject = new object();

    //    private static void CheckForUpdate()
    //    {
    //        _countChanges++;
    //        if (_countChanges >= 100)
    //        {
    //            CreateContext();
    //        }
    //    }

    //    private static void CreateContext()
    //    {
    //        lock (SyncObject)
    //        {
    //            var context = new KModelContainer1();
    //            _context?.Dispose();
    //            Interlocked.Exchange(ref _context, context);
    //            _countChanges = 0;
    //            //context.Database.Initialize(true);
    //        }
    //    }

    //    protected virtual void OnChanged()
    //    {
    //        Changed?.Invoke(this, EventArgs.Empty);
    //    }
    //}

    public abstract class Repository<T> where T: class
    {
        protected readonly IDbProvider Provider;
        protected Repository(IDbProvider provider)
        {
            Provider = provider;
        }

        protected Task Update(Action<KModelContainer1> action, bool notifySubscribers = true)
        {
            return Provider.Update(action, OnChanged, notifySubscribers);
        }

        protected KModelContainer1 Context => Provider.Context;

        public event EventHandler Changed;
        
        public void OnChanged()
        {
            Changed?.Invoke(this, EventArgs.Empty);
        }

        public abstract Task Add(T entity);

        public abstract Task Delete(T entity);

        public abstract Task Update(T entity);

        public abstract IEnumerable<T> GetAll { get; }

        public abstract T FindById(int id);
    }
}
